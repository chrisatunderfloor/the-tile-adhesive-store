/**
 * Remove Attributes plugin which removes all attributes from all elements in the jquery object
 */
$.fn.removeAttributes = function(keep) {
    return this.each(function() {
        var attributes = $.map(this.attributes, function(item) {
            return item.name;
        });
        var elem = $(this);
        $.each(attributes, function(i, item) {
            if(item !== keep) {
                elem.removeAttr(item);
            }
        });
    });
}

/**
 * Removes all empty elements, found in the jquery object, from the DOM
 */
$.fn.removeIfEmpty = function() {
    return this.each(function(){
        if($.trim($(this).text()) === '') {
            if($.trim($(this).html()) === '' || $.trim($(this).html()) === '&nbsp;') {
                $(this).remove();
            }
        }
    });
}

// Get rid of the crap we don't want
$('#search-page br, #search-page hr, #search-page style').remove();

// Remove unneccessary wrappers
$('#search-page font').children().unwrap();
$('#search-page center').children().unwrap();

// Swap remaining font tags for spans
$('#search-page font').each(function(index){
    $(this).replaceWith('<span>'+$.trim($(this).text())+'</span>');
});

// Remove all inline table attributes
$('table, tbody, tr, td, th', $('#search-page')).removeAttributes(['id','colspan','rowspan']);

// input sizes
$('#search-results input[size]').removeAttr('size');

// remove all image width and height attributes
$('#search-page img').each(function(index){
    $(this).removeAttr('width');
    $(this).removeAttr('height');
});

// remove border attribute from all elements
$('#search-results *[border]').removeAttr('border');

// b's to strongs
$('#search-page b').each(function(index){
    $(this).replaceWith('<strong>'+$.trim($(this).text())+'</strong>');
});

// all inline styles
$('#search-results *[style]').each(function(index){
    $(this).removeAttr('style');
});

$('#search-results span[id^="toggleopacity"] a').unwrap();

$('#search-page > form').attr('id','advanced-search-form');

$('#search-page > table:nth-of-type(1)').attr('id','search-stats');
$('#search-page > table:nth-of-type(2)').attr('id','search-results');
$('#search-page > table:nth-of-type(3)').attr('id','search-pagination');

$('#search-stats').replaceWith('<h1>Search Results</h1>');

// Replace all images with their bigger counterparts
/*$('#search-results img').each(function(){
    var urlPart = $(this).attr('src').match(/(.*\[ekm\]).*(\[ekm\].*)/);
    $(this).attr('src', urlPart[1]+'500x450'+urlPart[2]);
});*/

// Messy I know but it works for now.......
$('#search-page td').removeIfEmpty();
$('#search-page th').removeIfEmpty();
$('#search-page tr').removeIfEmpty();
$('#search-page tbody').removeIfEmpty();
$('#search-page table').removeIfEmpty();
$('#search-page td').removeIfEmpty();
$('#search-page th').removeIfEmpty();
$('#search-page tr').removeIfEmpty();
$('#search-page tbody').removeIfEmpty();
$('#search-page table').removeIfEmpty();
$('#search-page strong').removeIfEmpty();

$('#advanced-search-form > table > tbody > tr:nth-of-type(1), #advanced-search-form > table > tbody > tr:nth-of-type(13)').addClass('section-sub-title');
$('#search-results input[type="submit"]').wrap('<div class="button-wrapper"></div>');

$('#search-results img').closest('td').addClass('search-image');
$('#search-results img').closest('td').next('td').addClass('search-name');