$(document).ready(function () {
	$('.related-products-slider').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 2500,
		pauseOnHover: true,
		speed: 1000,
		responsive: [
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 479,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			}
		]
	});

	$('#tab-content dt').on('click', function () {
        var tab = $(this).data('tab');
        if ($(this).hasClass('active')) {
            $('[data-tab="' + tab + '"]').removeClass('active');
        }
        else {
            $('#tab-content dt, #tab-content dd').removeClass('active');
            $('[data-tab="' + tab + '"]').addClass('active');
            // Move currently active tab to top of screen if on mobile
            if($.browser.mobile == true) {
	            var offset = $('dt[data-tab="'+tab+'"]').offset().top;
	            if($(window).scrollTop() != offset){
	            	$('html, body').animate({
	            		scrollTop: offset
	            	}, 250);
	            }
	        }
        }
    });

    // Hide un-used tabs and their content on the product page if we are in the live environment
    if (window.location.hostname.indexOf('ekm') == -1 && window.location.hostname.indexOf('local') == -1) {
        $('#tab-content > dd').each(function (index, elem) {
            var html = $(elem).html().replace(/[\s\t\n]*/g, '');
            if (html == "") {
                $('#tab-content dd[data-tab="' + $(elem).attr('id') + '"], #tab-content dt[data-tab="' + $(elem).attr('id') + '"]').remove();
                $(elem).remove();
            }
        });
    }

    $('#tab-content [data-tab="details"]').addClass('active');

    //Show out-of-stock for all products wihtout add to cart button
    if (!$('#_EKM_PRODUCTADDCART').length) {
        $('#add-to-cart-button-container').css('display', 'none');
        $('#out-of-stock').css('display', 'block');
    }
});