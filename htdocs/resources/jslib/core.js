/**
* Create a google map block showing the office location
*/
if ($('#contact-map').length != 0)
{
	var mapOptions = {
		center: new google.maps.LatLng(51.586457, 0.471970),
		zoom: 12,
		disableDefaultUI: true,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		zoomControl: true,
		streetViewControl: true
	};

	map = new google.maps.Map(document.getElementById("contact-map"), mapOptions);
	var markerPos = new google.maps.LatLng(51.586457, 0.471970);

	var image = new google.maps.MarkerImage('/ekmps/shops/underfloorheat/resources/Design/marker.png',
		new google.maps.Size(130, 86),
		new google.maps.Point(0, 0),
		new google.maps.Point(20, 86));

	marker = new google.maps.Marker({
		position: markerPos,
		animation: google.maps.Animation.DROP,
		map: map,
		//icon: image
	});
}

$(document).ready(function(){
	$('.flexslider').each(function(index, elem){
		var config = {
			selector: '.slides > .slide',
			animation: 'slide',
			move: 1
		};
		if($(this).attr('id') == 'slideshow') {
			config.manualControls = '#slide-controls .control';
		} else {
			config.controlNav = false;
			config.directionNav = false;
			config.slideshowSpeed = 4500;
		}
		$(elem).flexslider(config);
	});

	$('.move-to-main').prependTo('#main-column')
		.css('height', 'auto');

	// Mobile menu show/hide
	$('#mobile-menu span').on('click', function(elem){
		$('#main-nav .main').toggleClass('active');
		$('#main-nav .main > li > a + ul').removeClass('active');
	});

	$('#main-nav .main > li').hover(
		function(e){
			$('#main-nav .main > li > a + ul').removeClass('active');
			$(this).children('a').next('ul').addClass('active');
		},
		function(e){
			$('#main-nav .main > li > a + ul').removeClass('active');
		});

	// Move mini cart to nav bar if we are on mobile
	if($.browser.mobile == true) {
		$('#main-nav .main > li > a').click(function(e){
			e.preventDefault();
			$('#main-nav .main > li > a + ul').removeClass('active');
			$(this).next('ul').addClass('active');
		});
		$('#side-nav > li > a').click(function(e){
			e.preventDefault();
			$('#side-nav > li > a + ul').removeClass('active');
			$(this).next('ul').addClass('active');
		});
		$('html').addClass('mobile');
	}

	// Enable hover functions for mini cart
	$('#mini-cart-container').hover(
		function () {
			$('#mini-cart').removeClass('hidden');
		},
		function () {
			$('#mini-cart').addClass('hidden');
		}
	);

	$('.price-value strong').each(function(i, e){
		splitPrice = $(e).text()
			.trim()
			.replace(/^£/,'')
			.split('.');
		$(e).text(splitPrice[0]);
		$(e).next('div')
			.children('.pence')
			.text('.'+splitPrice[1]);
	});

	$('#side-nav > li').hover(
		function(e){
			$('#side-nav > li > a + ul').removeClass('active');
			$(this).children('a').next('ul').addClass('active');
		},
		function(e){
			$('#side-nav > li > a + ul').removeClass('active');
		});

	$('.accordion .heading').click(function () {
        if ($(this).hasClass('active')) {
            $('.accordion .heading').removeClass('active');
        }
        else {
            $('.accordion .heading').removeClass('active');
            $(this).addClass('active');
            if($.browser.mobile == true) {
	            var offset = $(this).offset().top;
	            if($(window).scrollTop() != offset){
	            	$('html, body').animate({
	            		scrollTop: offset
	            	}, 250);
	            }
	        }
        }
    });

    $('.accordion-control [data-leaf]').click(function () {

        var id = $(this).closest('.accordion-control').data('id');
        var val = parseInt($(this).data('leaf'));

        var title = $('#'+id+' h2:nth-of-type('+val+')');
        if (title.hasClass('active')) {
            $('#'+id+' .heading').removeClass('active');
        }
        else {
            $('#'+id+' .heading').removeClass('active');
            title.addClass('active');
            if($.browser.mobile == true) {
	            var offset = title.offset().top;
	            if($(window).scrollTop() != offset){
	            	$('html, body').animate({
	            		scrollTop: offset
	            	}, 250);
	            }
	        }
        }
    });

    // Wrap the shopping cart page content in a #main-column div so the
    // trustpilot reviews appear neatly in the side bar
    if($('body').data('page') == 'CART_PAGE') {
    	$('#body-content').children().not($('#side-bar')).wrapAll('<div id="main-column">');
    }
});
