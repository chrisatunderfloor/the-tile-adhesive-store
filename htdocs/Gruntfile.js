module.exports = function(grunt) {

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		concat: {
			options: {},
			dist: {
				src: [
					'resources/jslib/jquery-3.1.1.min.js',
					'resources/jslib/jquery.flexslider.js',
					'resources/jslib/detectmobilebrowser.js',
					'resources/jslib/max-height.js',
					'resources/jslib/search.js',
					'resources/jslib/slick.js',
					'resources/jslib/product-page.js',
					'resources/jslib/core.js'
				],
				dest: 'resources/js/main.js'
			}
		},
		uglify: {
			my_target: {
				files: {
					'resources/js/main.js': [
						'node_modules/jquery/dist/jquery.min.js',
						'node_modules/bootstrap/dist/js/bootstrap.min.js',
						'resources/jslib/core.js',
						'resources/jslib/max-height.js',
						'resources/jslib/search.js'
					]
				}
			}
		},
		sass: {
			dist: {
				options: {
					style: 'expanded',
				},
				files: [{
					expand: true,
					cwd: 'resources/scss',
					src: ['*.scss'],
					dest: 'resources/css',
					ext: '.css'
				}]
			}
		},
		watch: {
			files: [
				'**/*.scss',
				'**/jslib/*.js'
			],
			tasks: 'default',
			options: {
				livereload: true
			}
		}
	});
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');

	grunt.registerTask('default', ['sass','concat']);
};