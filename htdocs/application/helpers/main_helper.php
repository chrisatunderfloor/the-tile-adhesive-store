<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// ------------------------------------------------------------------------

/**
 * CodeIgniter Swan Helpers
 *
 * @package		Swan Web
 * @subpackage	Helpers
 * @category	Helpers
 * @author		Swan Web
 * @link		http://swan-web.com
 */
// ------------------------------------------------------------------------

/*
 * 	Echo's out <pre>'ed value
 *
 * @param string $value
 * @param string $name
 */
if (!function_exists('echoPre'))
{

    function echoPre($value, $print = true)
    {
        if ($value)
        {
            $output = "<pre>";
            $output .= print_r($value, true);
            $output .= "</pre>";
            if ($print)
            {
                echo $output;
            }
            else
            {
                return $output;
            }
        }
        return false;
    }

}

/* End of file swan_helper.php */
/* Location: ./system/helpers/swan_helper.php */