<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="lt-ie9"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="lt-ie9"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="lt-ie9"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>[ekm:pagetitle]</title>

	<meta name="keywords" content="[ekm:metakeywords][/ekm:metakeywords]" />
	<meta name="description" content="[ekm:metadescription][/ekm:metadescription]" />

	<link rel="apple-touch-icon" href="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/Design/apple-touch-icon.png"/>

	<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
	<link rel="stylesheet" href="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/Styles/style.css?v=4">

	<!-- TrustBox script -->
	<script type="text/javascript" src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js" async></script>
	<!-- End Trustbox script -->

</head>

<body>
	<header>
		<div id="header-logo-strip" class="container">
			<div id="logo">
				<a href="/"><img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/Design/logo.svg"/></a>
			</div>
			<div id="telephone-header">
				<h1>Cheapest underfloor heating <span>online!</span></h1>
				<tel>0800 141 2643</tel>
				<p>Call us today for the lowest online prices</p>
			</div>
			<div id="search-form-container">
				<div id="social-contact">
					<a href="/request-a-quote-239-w" class="links">Request A Quote</a>
					<span class="links">&nbsp;|&nbsp;</span>
					<a href="/contact-us-233-w" class="links">Support</a>
					<div class="social-icons">
						<a target="_blank" href="https://www.facebook.com/FloorHeatingDirect">
							<img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/Design/social-facebook.jpg"/>
						</a>
						<a target="_blank" href="https://twitter.com/FloorHeatDirect">
							<img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/Design/social-twitter.jpg"/>
						</a>
					</div>
				</div>
				<!--<form name="searchbox" method="post" id="search-box" action="index.asp?function=search">-->
				<form name="searchbox" method="post" id="search-box" action="search-225-w">
					<input type="text" name="search" size="20">
					<button>SEARCH</button>
				</form>
				<!-- TrustBox widget - Micro TrustScore -->
				<div class="trustpilot-widget" data-locale="en-GB" data-template-id="5419b637fa0340045cd0c936" data-businessunit-id="4eca53ef0000640005119918" data-style-height="20px" data-style-width="100%" data-theme="light">
				  <a href="https://uk.trustpilot.com/review/www.underfloorheating-direct.com" target="_blank">Trustpilot</a>
				</div>
				<!-- End TrustBox widget -->
			</div>
		</div>
	</header>

	<nav id="main-nav" role="navigation">
		<div class="container">
			<div id="mobile-menu"><span></span></div>
			<ul class="main">
				<li>
					<a href="electric-underfloor-heating-355-c.asp" class="has-child">Electric Underfloor Heating</a>
					<ul>
						<li class="mobile-only"><a href="electric-underfloor-heating-355-c.asp">All Electric Underfloor Heating</a></li>
						<li><a href="underfloor-heating-mats-362-c.asp">Underfloor Heating Mats</a></li>
						<li><a href="underwood-foil-heating-mats-367-c.asp">Underwood Foil Heating Mats</a></li>
						<li><a href="loose-cable-heating-systems-366-c.asp">Loose Cable Heating Systems</a></li>
						<li><a href="undercarpet-foil-heating-mats-369-c.asp">Undercarpet Foil Heating Mats</a></li>
						<li><a href="inscreed-heating-systems-368-c.asp">Inscreed Heating Systems</a></li>
						<li><a href="insulation-boards-371-c.asp">Insulation Boards</a></li>
						<li><a href="thermostats-404-c.asp">Thermostats</a></li>
						<li><a href="electric-accessories-372-c.asp">Electric Accessories</a></li>
					</ul>
				</li>
				<li>
					<a href="water-underfloor-heating-359-c.asp" class="has-child">Water Underfloor Heating</a>
					<ul>
						<li class="mobile-only"><a href="water-underfloor-heating-359-c.asp">All Water Underfloor Heating</a></li>
						<li><a href="single-zone-heating-kits-373-c.asp">Single Zone Heating Kits</a></li>
						<li><a href="high-output-kit-374-c.asp">High Output Kit</a></li>
						<li><a href="multi-zone--large-area-heating-kits-375-c.asp">Multi Zone & Large Area Heating Kits</a></li>
						<li><a href="low-profile-kits-434-c.asp">Low Profile Kits</a></li>
						<li><a href="insulation-boards-361-c.asp">Insulation Boards</a></li>
						<li><a href="thermostats-386-c.asp">Thermostats</a></li>
						<li><a href="water-heating-accessories-360-c.asp">Water Heating Accessories</a></li>
					</ul>
				</li>
				<li>
					<a href="insulation-boards-406-c.asp" class="has-child">Insulation Boards</a>
					<ul>
						<li class="mobile-only"><a href="insulation-boards-406-c.asp">All Insulation Boards</a></li>
						<li><a href="electric-408-c.asp">Electric</a></li>
						<li><a href="water-409-c.asp">Water</a></li>
					</ul>
				</li>
				<li>
					<a href="thermostats-405-c.asp" class="has-child">Thermostats</a>
					<ul>
						<li class="mobile-only"><a href="thermostats-405-c.asp">All Thermostats</a></li>
						<li><a href="electric-414-c.asp">Electric</a></li>
						<li><a href="water-415-c.asp">Water</a></li>
					</ul>
				</li>
				<li>
					<a href="tile-adhesive-and-levellers-407-c.asp">Tile Adhesive and Levellers</a>
				</li>
				<li>
					<a href="wetrooms-435-c.asp" class="has-child">Wetroom Shower Trays</a>
					<ul>
						<li><a href="wetroom-shower-trays-436-c.asp">Wetroom Shower Trays</a></li>
						<li><a href="wetroom-drains-437-c.asp">Wetroom Drains</a></li>
					</ul>
				</li>
			</ul>
			<div id="mini-cart-container">
				<span id="minicart_item_count">12</span>
				<a href="#" id="mini-cart-trigger"><span></span>Cart</a>
				<div id="mini-cart" class="hidden">
					<table>
						<tr>
							<th>Item</th>
							<th>Qty</th>
							<th>Price</th>
						</tr>
						<tr>
							<td><a href="duct-tape-6691-p.asp">Cable Kit</a></td>
							<td>3</td>
							<td>£287.99</td>
						</tr>
						<tr>
							<td><a href="duct-tape-6691-p.asp">Underwood mat kit</a></td>
							<td>2</td>
							<td>£123.99</td>
						</tr>
						<tr>
							<td><a href="duct-tape-6691-p.asp">Insulation board</a></td>
							<td>1</td>
							<td>£7.99</td>
						</tr>
						<tr>
							<td><a href="duct-tape-6691-p.asp">Multi-room water kit</a></td>
							<td>2</td>
							<td>£27.49</td>

						</tr>
					</table>
					<span id="mini-cart-total"><strong>Total</strong><span id="ekm_minicart_total">£1234.00</span></span>
					<a href="checkout-226-w.asp">Proceed to checkout</a>
				</div>
			</div>
		</div>
	</nav>

	<nav id="sub-nav">
		<div class="container">
			<a href="free-delivery-234-w.asp" class="sub-nav-item">Free delivery to UK &amp; Europe</a>
			<a href="shop-with-confidence-241-w.asp" class="sub-nav-item">Shop with confidence</a>
			<a href="returns-policy-240-w.asp" class="sub-nav-item">30 day returns policy</a>
			<a href="terms-and-conditions-242-w.asp" class="sub-nav-item">Terms and conditions</a>
		</div>
	</nav>

	<div id="body-content" class="container">
		[ekm:content]
	</div>

	<div id="trustpilot-mini-strip">
		<div class="container">
			<!-- TrustBox widget - Horizontal -->
			<div class="trustpilot-widget" data-locale="en-GB" data-template-id="5406e65db0d04a09e042d5fc" data-businessunit-id="4eca53ef0000640005119918" data-style-height="28px" data-style-width="100%" data-theme="light">
				<a href="https://uk.trustpilot.com/review/www.underfloorheating-direct.com" target="_blank">Trustpilot</a>
			</div>
			<!-- End TrustBox widget -->
		</div>
	</div>

	<div class="footer-holder">
		<footer>
			<div class="container">
				<a href="/trade-clearance-417-c.asp" id="clearance-corner-flash"><img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/Design/stock-clearance-corner-flash.png"/></a>
				<div class="footer-blocks">
					<div class="footer-block">
						<h4>Electric underfloor heating</h4>
						<ul>
							<li><a href="underfloor-heating-mats-348-c.asp">Underfloor Heating Mats</a></li>
							<li><a href="underfloor-heating-cables-349-c.asp">Underfloor Heating Cables</a></li>
							<li><a href="underwood-heating-kit-22193-p.asp">Underwood Heating Kit</a></li>
							<li><a href="under-carpet--vinyl-heating-kit-22278-p.asp">Under Carpet / Vinyl Heating Kit</a></li>
							<li><a href="in-screed-heating-kit-22627-p.asp">In Screed Heating Kit</a></li>
							<li><a href="insulation-boards-353-c.asp">Insulation Boards</a></li>
							<li><a href="electric-accessories-17-c.asp">Accessories</a></li>
							<li><a href="thermostats-386-c.asp">Thermostats</a></li>
						</ul>
					</div>
					<div class="footer-block">
						<h4>Water underfloor heating</h4>
						<ul>
							<li><a href="water-underfloor-heating---standard-room-kit-22682-p.asp">Standard Room Kit</a></li>
							<li><a href="water-underfloor-heating---multiple-room-kit-22711-p.asp">Multiple Room Kit</a></li>
							<li><a href="water-underfloor-heating---high-output-kit-22753-p.asp">High Output Kit</a></li>
							<li><a href="low-profile-water-underfloor-heating---single-room-kit-22782-p.asp">Low Profile - Single Room Kit</a></li>
							<li><a href="low-profile-water-underfloor-heating---multiple-room-kit-22730-p.asp">Low Profile - Multiple Room Kit</a></li>
							<li><a href="water-accessories-18-c.asp">Water Heating Accessories</a></li>
						</ul>
					</div>
					<div class="footer-block">
						<h4>Services</h4>
						<ul>
							<li><a href="free-delivery-234-w.asp">Free delivery</a></li>
							<li><a href="shop-with-confidence-241-w.asp">Shop with confidence</a></li>
							<li><a href="returns-policy-240-w.asp">Returns policy</a></li>
							<li><a href="terms-and-conditions-242-w.asp">Terms & conditions</a></li>
							<li><a href="lifetime-warranty-237-w.asp">Lifetime Warranty</a></li>
						</ul>
					</div>
					<div class="footer-block">
						<h4>Help & Advice</h4>
						<ul>
							<li><a href="why-use-us-243-w.asp">Why use us</a></li>
							<li><a href="how-to-install-235-w.asp">How to install</a></li>
							<li><a href="confused-224-w.asp">Confused?</a></li>
							<li><a href="contact-us-233-w.asp">Contact us</a></li>
							<li><a href="how-to-measure-up-236-w.asp">Measuring up</a></li>
							<li><a href="request-a-quote-239-w.asp">Request a Quote</a></li>
						</ul>
					</div>
				</div>
				<div class="social-icons">
					<a target="_blank" href="https://www.facebook.com/FloorHeatingDirect">
						<img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/Design/social-facebook.jpg"/>
					</a>
					<a target="_blank" href="https://twitter.com/FloorHeatDirect">
						<img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/Design/social-twitter.jpg"/>
					</a>
				</div>
			</div>
		</footer>



		<div id="footer-contact-details" class="container">
			<div id="logo-container">
				<img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/Design/logo.svg"/>
			</div>
			<div id="contact-details-container">
				<tel>0800 141 2643</tel>
				<address>Unit 5, Sovereign Park, Cranes Farm Road, Basildon, Essex, SS14 3AD</address>
			</div>
			<p>UFH Direct is an online company, unfortunately we do not have the facility to collect goods from our distribution centre, all orders must be placed online.</p>
		</div>
	</div>

	<script src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/Other/main.js"></script>
	<script src="//chris.ufhd.heatstore.local:35729/livereload.js"></script>

</body>
</html>