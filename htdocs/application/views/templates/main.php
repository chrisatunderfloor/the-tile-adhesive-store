<!DOCTYPE html>

<html lang="en-GB">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="keywords" content="[ekm:metakeywords][/ekm:metakeywords]">
	<meta name="description" content="[ekm:metadescription][/ekm:metadescription]">

	[ekm:if]
	    iftype='IS_MOBILE_BROWSER';
	    ifthen='<meta name="viewport" content="width=device-width, initial-scale=1">';
	    ifelse='';
	[/ekm:if]

	[ekm:favicon][/ekm:favicon]

	<title>[ekm:pagetitle][/ekm:pagetitle]</title>
	<!-- EKM Theme:  Hitbox (1549) -->

	<script src="https://cdn.ekmsecure.com/js/jquery/latest/jquery.min.js"></script>

	[ekm:if]
		iftype='EQUALS';
		ifvalue='[ekm:industry_variant_name][/ekm:industry_variant_name]';
		ifvalue2='Electronics';
		ifthen='<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">';
		ifelse='';
	[/ekm:if]

	[ekm:if]
		iftype='EQUALS';
		ifvalue='[ekm:industry_variant_name][/ekm:industry_variant_name]';
		ifvalue2='Animals and Pets';
		ifthen='<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">';
		ifelse='';
	[/ekm:if]

	[ekm:if]
		iftype='EQUALS';
		ifvalue='[ekm:industry_variant_name][/ekm:industry_variant_name]';
		ifvalue2='DIY and Construction';
		ifthen='<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">';
		ifelse='';
	[/ekm:if]

	<link href="https://cdn.ekmsecure.com/font-awesome/latest/fa.css" rel="stylesheet">

	<link href="/ekmps/designs/assets/master/1549/css/responsive-grid.css" rel="stylesheet">
    <link href="/ekmps/designs/assets/master/1549/css/styles.dymc.css" rel="stylesheet">

    <link href="/ekmps/shops/d534fa/resources/Styles/style.css" rel="stylesheet">

    [ekm:cart_custom_css]<link rel="stylesheet" href="/ekmps/designs/assets/master/1549/css/cart-page.css">[/ekm:cart_custom_css]

    <style>
	    body {
	        background: [ekm:background][/ekm:background];
	    }
    </style>
</head>


<body class="push-drawer">


<header>
	<section class="site-header">
		<div class="flex-grid flex-align-center header-elements container">
			[ekm:showdata]
				tag_hide_toggle='yes';
				font_formatting='no';

				' Data to Show
				data='categories';
				location='0';

				' Basic Layout & Navigation
				cols='1';
				rows='96';
				page_navigation='no';

				' Advanced Data Manipulation
				orderby='orderlocation';
				skiprecords='0';
				excludelist='';
				includelist='';

				' Navigation Options when logged in
				editbuttons='YES';
				counterreset='';

				' Output Format
				output_start='
					<div class="flex-col nav-btn nav-col-m">
						<svg version="1.1" class="nav-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 width="20px" height="18px" viewBox="4 4.5 19 17" enable-background="new 4 4.5 19 17" xml:space="preserve">
							<path d="M21.333,18.407H5.667c-0.645,0-1.167,0.58-1.167,1.297S5.022,21,5.667,21h15.666
							c0.645,0,1.167-0.58,1.167-1.297S21.978,18.407,21.333,18.407z"/>
							<path d="M21.333,5H5.667C5.022,5,4.5,5.58,4.5,6.297s0.522,1.297,1.167,1.297h15.666c0.645,0,1.167-0.58,1.167-1.297
							S21.978,5,21.333,5z"/>
							<path d="M21.333,11.703H5.667C5.022,11.703,4.5,12.284,4.5,13s0.522,1.297,1.167,1.297h15.666
							c0.645,0,1.167-0.58,1.167-1.297S21.978,11.703,21.333,11.703z"/>
						</svg><!-- /.nav-icon -->
					</div><!-- /.flex-col .nav-btn .nav-col-m -->

					<nav class="drawer drawer-left m-padding-full-m">
						<div class="nav-accordion-m xl-margin-top-m">
							<ul class="ul-reset">';
				output_row_start='';
				output_column_start='';
				output_item='
								{name}
								<li class="m-margin-left-m m-margin-right-m">
									<a href="[url]" class="m-padding-full-m">[name-NOEDIT-NOLINK][editbuttons]</a><!-- /.m-padding-full-m -->
										{ekm:dynamic-dropdown}
											[ekm:nested_showdata]
												data="categories";
												location="[id]";
												cols="1";
												rows="96";
												page_navigation="no";
												orderby="orderlocation, name";
												font_formatting="No";

												output_start="
													<ul class="ul-reset">
												";
												output_row_start="";
												output_column_start="";
												output_item="<li>[nested_name]</li>";
												output_column_end="";
												output_row_end="";
												output_end="
														<li><a href="[url]">View All</a></li>
													</ul><!-- /.ul-reset -->
												";
												element_replace="null";
											[/ekm:nested_showdata]
									{/ekm:dynamic-dropdown}
								</li><!-- /.m-margin-left-m .m-margin-right-m -->
								{/name}
				';
				output_column_end='';
				output_row_end='';
				output_end='
							</ul><!-- /.ul-reset -->
						</div><!-- /.nav-accordion-m .xl-margin-top-m -->

						<div class="close-nav-btn">
							<svg version="1.1" class="close-icon" x="0px" y="0px" width="18px" height="18px" viewBox="312 387 18 18" enable-background="new 312 387 18 18" xml:space="preserve">
								<path d="M312.54,404.46c0.36,0.36,0.84,0.54,1.26,0.54c0.42,0,0.9-0.18,1.26-0.54l5.94-5.94l5.94,5.94
								c0.36,0.36,0.84,0.54,1.26,0.54c0.42,0,0.9-0.18,1.26-0.54c0.72-0.72,0.72-1.86,0-2.52l-5.94-5.94l5.94-5.94
								c0.72-0.72,0.72-1.86,0-2.52c-0.72-0.72-1.86-0.72-2.52,0l-5.94,5.94l-5.94-5.94c-0.72-0.72-1.86-0.72-2.52,0
								c-0.72,0.72-0.72,1.86,0,2.52l5.94,5.94l-5.94,5.94C311.82,402.66,311.82,403.8,312.54,404.46z"/>
							</svg><!-- /.close-icon -->
						</div><!-- /.close-nav-btn -->
					</nav><!-- /.drawer .drawer-left .m-padding-full-m -->
				';
				' Element Replace
				element_replace='null';
			[/ekm:showdata]

			[ekm:element]
				element_reference='logo';
				edit_button='YES';
				output_start='
					<div class="flex-col logo m-logo-col d-col-5">
						<span><a href="index.asp">
				';
				output_end='
						</a></span>
					</div><!-- /.flex-col .logo .m-logo-col .d-col-3 -->
				';
				element_name_default='Your Logo';
				output_default='<img src="/ekmps/designs/assets/master/1549/[ekm:industry_variant_name][/ekm:industry_variant_name]/images/logo.svg" alt="[ekm:sitename] logo">';
			[/ekm:element]

			[ekm:searchbox]
				show_search_term='yes';
				search_field_placeholder='Search for products...';
				search_button_value='';
				output_start='<div class="flex-col flex-display flex-justify-center search-box d-col-4 m-col-12">';
				layout='
					[search-field]

					<button class="ekmps-search-button" src="" value="" alt="" type="submit">
						<svg version="1.1" class="search-button-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 width="25px" height="25px" viewBox="2.5 2.5 25 25" enable-background="new 2.5 2.5 25 25" xml:space="preserve">
							<path d="M26.633,24.863l-5.796-5.796c3.03-3.906,2.757-9.562-0.828-13.148C18.127,4.037,15.625,3,12.964,3
							S7.801,4.037,5.919,5.918C4.036,7.8,3,10.302,3,12.963s1.036,5.164,2.917,7.045c1.882,1.883,4.384,2.919,7.045,2.919
							c2.239,0,4.363-0.736,6.103-2.09l5.796,5.796C25.106,26.879,25.427,27,25.747,27c0.321,0,0.64-0.121,0.885-0.366
							C27.122,26.145,27.122,25.352,26.633,24.863z M7.689,18.239c-1.408-1.408-2.184-3.282-2.184-5.274S6.282,9.099,7.69,7.691
							s3.28-2.184,5.274-2.184c1.992,0,3.865,0.775,5.274,2.184c2.908,2.907,2.908,7.639,0,10.548c-1.408,1.408-3.282,2.186-5.274,2.186
							C10.971,20.423,9.099,19.647,7.689,18.239z"/>
						</svg><!-- /.search-button-icon -->
					</button><!-- /.ekmps-search-button -->
				';
				output_end='</div><!-- /.flex-col .flex-display .flex-justify-center .search-box .d-col-6 .m-col-12 -->';
			[/ekm:searchbox]

			<div class="flex-col flex-display header-icons d-col-3 m-icons-col">
				<div class="flex-grid flex-align-center">
					[ekm:if]
					    iftype='FEATURE_ACTIVE';
					    ifvalue='FEATURE-CUSTOMERLOGIN';
					    ifthen='
					    	<div class="flex-col account-element l-margin-right-d m-margin-right-m">

					    		<a class="account-btn-wrapper" href="[ekm:login_page_link] url_only='yes'; [/ekm:login_page_link]">
									<div class="account-btn">
										<svg version="1.1" class="account-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
											width="28.888px" height="30px" viewBox="0 0 28.888 30" enable-background="new 0 0 28.888 30" xml:space="preserve">
											<path d="M19.902,14.95c1.849-1.546,3.027-3.865,3.027-6.465C22.928,3.808,19.121,0,14.444,0S5.96,3.766,5.96,8.443
											c0,2.607,1.183,4.943,3.041,6.501c-5.257,1.97-9,7.041-9,12.986V30h2.897v-2.069c0-6.042,4.925-10.968,10.968-10.968h1.159
											c6.042,0,10.968,4.925,10.968,10.968V30h2.897v-2.069C28.888,21.991,25.152,16.924,19.902,14.95z M14.444,2.897
											c3.063,0,5.587,2.442,5.587,5.546c0,3.104-2.525,5.587-5.587,5.587s-5.587-2.442-5.587-5.546S11.381,2.897,14.444,2.897z"/>
										</svg><!-- /.account-icon -->
									</div><!-- /.account-btn -->
								</a><!-- /.account-btn-wrapper .account-logged-in -->

							</div><!-- /.flex-col account-element .l-margin-right-d .m-margin-right-m -->
					    ';
					    ifelse='';
					[/ekm:if]


		            <a class="flex-col minicart-element flex-grid flex-align-center" href="index.asp?function=CART">
						<svg version="1.1" class="minicart-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			            	width="26.024px" height="30px" viewBox="0 0 24.024 30" enable-background="new 0 0 24.024 30" xml:space="preserve">
			                <path d="M24.015,28.475L22.889,8.747c-0.049-0.779-0.676-1.365-1.459-1.365h-3.591V6.394
			                C17.839,2.868,14.97,0,11.445,0C9.731,0,8.124,0.664,6.919,1.869S5.051,4.681,5.051,6.394v0.988H1.459
			                C0.69,7.382,0.049,7.998,0,8.785l0,0.003l0,0.003L-0.777,28.49l0,0.006v0.006c0,0.374,0.138,0.754,0.379,1.043l0.009,0.01
			                l0.009,0.009C-0.087,29.858,0.261,30,0.682,30h21.874c0.404,0,0.782-0.155,1.063-0.436C23.917,29.266,24.061,28.87,24.015,28.475z
			                M7.97,6.355c0-1.916,1.559-3.475,3.475-3.475c0.936,0,1.811,0.359,2.463,1.012c0.652,0.652,1.012,1.527,1.012,2.463v0.988H7.97
			                L7.97,6.355L7.97,6.355z M5.383,16.596h2.331h0.294v-0.294v-6h6.91v6.039v0.294h0.294h2.331h0.294V16.34v-6.039h2.226l0.976,16.74
			                H2.232l0.676-16.779h2.182v6.039v0.294L5.383,16.596L5.383,16.596L5.383,16.596z"/>
						</svg><!-- /.minicart-icon -->

						[ekm:minicart]
							style='custom';
							nocartmessage='neverhide';
							maxcharacters='15';
							imagewidth='50';
							imageheight='50';
							output_start='';
							output_end='';
							output_contents_start='';
							output_contents_end='';
							output_cart_contents='';
							output_totals='[total_message]';
							cart_totals_message_empty='';
							cart_totals_message_single_item='<div class="cart-btn-item-count">[item_count]</div><!-- /.cart-btn-item-count -->';
							cart_totals_message_multiple_items='<div class="cart-btn-item-count">[item_count]</div><!-- /.cart-btn-item-count -->';
						[/ekm:minicart]
					</a><!-- /.flex-col .minicart-element .flex-grid .flex-align-center -->
	            </div><!-- /.flex-grid .flex-align-center -->
			</div><!-- /.flex-col .flex-display .header-icons .d-col-3 .m-icons-col -->
		</div><!-- /.flex-grid .flex-align-center .header-elements .container -->

		[ekm:showdata]
			tag_hide_toggle='yes';
			font_formatting='no';

			' Data to Show
			data='categories';
			location='0';

			' Basic Layout & Navigation
			cols='1';
			rows='96';
			page_navigation='no';

			' Advanced Data Manipulation
			orderby='orderlocation';
			skiprecords='0';
			excludelist='';
			includelist='';

			' Navigation Options when logged in
			editbuttons='YES';
			counterreset='';

			' Output Format
			output_start='
				<nav class="main-nav container nav-d">
					<ul class="flex-grid flex-justify-center flex-wrap-wrap ul-reset">';
			output_row_start='';
			output_column_start='';
			output_item='
					{name}
					<li class="flex-col">[name]
						{ekm:dynamic-dropdown}
							<section class="mega-menu">
								<div class="container">
									[ekm:nested_showdata]
										data="categories";
										location="[id]";
										cols="1";
										rows="96";
										page_navigation="no";
										orderby="orderlocation, name";
										font_formatting="No";

										output_start="<ul class="flex-grid ul-reset m-padding-bottom-d m-padding-top-d">";
										output_row_start="";
										output_column_start="";
										output_item="
											<li class="flex-col d-col-2 t-col-3 mega-menu-item">[nested_name]</li><!-- /.flex-col .d-col-2 .t-col-3 .mega-menu-item -->
										";
										output_column_end="";
										output_row_end="";
										output_end="

										<li class="flex-col d-col-2 t-col-3 mega-menu-item"><a href="[url]">View All</a></li><!-- /.flex-col .d-col-2 .t-col-3 .mega-menu-item -->
										</ul><!-- /.flex-grid .ul-reset .m-padding-bottom-d .m-padding-top-d -->";
										element_replace="null";
									[/ekm:nested_showdata]
								</div><!-- /.container -->
							</section><!-- /.mega-menu -->
						{/ekm:dynamic-dropdown}
					</li><!-- /.flex-col -->
					{/name}
			';
			output_column_end='';
			output_row_end='';
			output_end='
					</ul><!-- /.flex-grid .flex-justify-center .flex-wrap-wrap .ul-reset -->
				</nav><!-- /.main-nav .container .nav-d -->
			';
			' Element Replace
			element_replace='null';
		[/ekm:showdata]
	</section><!-- /.site-header -->



<section class="flex-grid container-narrow m-padding-left-m m-padding-right-m usp-bar">
	[ekm:element]
	  	show='no';
	  	allowed_types='text, html';

	  	element_reference='usp-box-1';
	  	edit_button='YES';
	  	output_start='<div class="flex-display flex-col flex-justify-center usp-box usp-box-m usp-1">';
	  	output_end='</div><!-- /.flex-display .flex-col .flex-justify-center .usp-box .usp-box-m .usp-1 -->';
	  	element_name_default='USP BOX 1';
	  	output_default='
	  		<span>
				<svg version="1.1" class="usp-heart-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					width="24.36px" height="22px" viewBox="0 0 24.36 22" enable-background="new 0 0 24.36 22" xml:space="preserve">
					<path d="M21.619,2.835c-1.124-1.173-2.626-1.818-4.231-1.818s-3.107,0.645-4.231,1.818l-0.969,1.013L11.206,2.82
					C10.154,1.689,8.612,1.025,6.978,1H6.976H6.975C5.371,1,3.869,1.646,2.744,2.82C1.622,3.99,1.004,5.539,1.002,7.18
					c-0.001,1.641,0.615,3.19,1.734,4.361l8.792,9.193C11.689,20.903,11.926,21,12.177,21s0.488-0.097,0.65-0.266
					c1.477-1.542,2.954-3.083,4.43-4.624c1.454-1.518,2.908-3.036,4.363-4.555c1.122-1.17,1.739-2.719,1.739-4.36
					C23.358,5.553,22.74,4.005,21.619,2.835z M20.328,10.312c-1.396,1.457-2.792,2.913-4.189,4.37c-1.32,1.376-2.639,2.753-3.958,4.129
					l-8.146-8.505c-1.648-1.723-1.648-4.523,0-6.242c0.798-0.832,1.842-1.29,2.94-1.29c1.094,0,2.135,0.458,2.932,1.29l1.628,1.702
					c0.161,0.169,0.398,0.266,0.649,0.266s0.488-0.097,0.65-0.266l1.621-1.694c0.797-0.832,1.84-1.29,2.936-1.29s2.139,0.458,2.936,1.29
					C21.977,5.791,21.977,8.591,20.328,10.312z"/>
				</svg><!-- /.usp-heart-icon -->
			</span>
			<span><strong>Save £££'s</strong> With Our <strong>Loyalty</strong> Scheme!</span>
	  	';
	[/ekm:element]

	[ekm:element]
	  	show='no';
	  	allowed_types='text, html';

	  	element_reference='usp-box-2';
	  	edit_button='YES';
	  	output_start='<div class="flex-display flex-col flex-justify-center usp-box usp-box-m usp-2">';
	  	output_end='</div><!-- /.flex-display .flex-col .flex-justify-center .usp-box .usp-box-m .usp-2 -->';
	  	element_name_default='USP BOX 2';
	  	output_default='
	  		<span>
				<svg version="1.1" class="usp-truck-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					width="34.52px" height="22px" viewBox="0 0 34.52 22" enable-background="new 0 0 34.52 22" xml:space="preserve">
					<path d="M27.26,7.53h-1.747c-0.326,0-0.592,0.266-0.592,0.591v2.493c0,0.326,0.266,0.591,0.592,0.591h3.52
					c0.326,0,0.592-0.266,0.591-0.591c0-0.123-0.038-0.242-0.109-0.342l-1.773-2.493C27.631,7.623,27.451,7.53,27.26,7.53z
					 M26.104,8.713h0.851l0.932,1.31h-1.783L26.104,8.713L26.104,8.713z"/>
					<path d="M33.486,11.607c-0.004-0.013-0.008-0.027-0.012-0.037v-0.022l-0.011-0.01
					c-0.037-0.096-0.09-0.183-0.155-0.253l-3.926-5.448c-0.152-0.212-0.4-0.338-0.661-0.339l-4.682-0.004V3.416
					C24.041,2.084,22.957,1,21.624,1H3.419L3.352,1.001v0.001c-1.302,0.035-2.35,1.105-2.35,2.415V16.18
					c0,1.332,1.084,2.417,2.417,2.417H5.64C6.067,20.015,7.385,21,8.866,21c1.481,0,2.799-0.985,3.227-2.403h12.738
					C25.259,20.015,26.576,21,28.057,21s2.799-0.985,3.226-2.403h0.236c1.102,0,1.999-0.897,1.999-1.999v-4.651
					C33.531,11.831,33.52,11.717,33.486,11.607z M31.888,16.597c0,0.207-0.163,0.37-0.37,0.37h-0.159
					c-0.316-1.574-1.695-2.709-3.303-2.709s-2.986,1.135-3.303,2.709H24.04V7.126l4.264,0.003l3.585,4.974L31.888,16.597L31.888,16.597z
					 M29.795,17.631c0,0.976-0.764,1.74-1.74,1.74c-0.975,0-1.739-0.764-1.739-1.74c0-0.978,0.764-1.743,1.739-1.743
					C29.031,15.888,29.795,16.654,29.795,17.631z M2.631,3.417c0-0.441,0.346-0.787,0.787-0.787h18.205c0.441,0,0.787,0.346,0.787,0.787
					v13.55H12.168c-0.316-1.574-1.695-2.709-3.303-2.709s-2.987,1.135-3.302,2.709H3.419c-0.441,0-0.787-0.346-0.787-0.787L2.631,3.417
					L2.631,3.417z M10.604,17.631v0.038c-0.02,0.954-0.785,1.701-1.739,1.701c-0.971,0-1.735-0.761-1.739-1.738
					c0-0.978,0.764-1.743,1.739-1.743h0.079l0,0C9.881,15.93,10.604,16.68,10.604,17.631z"/>
					<path d="M14.086,7.964h0.009h6.27l0,0c0.385,0,0.702-0.313,0.706-0.698c0.005-0.39-0.308-0.711-0.699-0.715h-6.277
					c-0.385,0-0.702,0.313-0.706,0.698c-0.003,0.188,0.069,0.367,0.201,0.502C13.722,7.885,13.898,7.961,14.086,7.964z M14.095,7.895
					L14.095,7.895L14.095,7.895L14.095,7.895z"/>
					<path d="M11.226,10.825h9.138c0.385,0,0.702-0.313,0.706-0.698c0.003-0.188-0.069-0.367-0.201-0.502
					c-0.132-0.135-0.309-0.211-0.497-0.213h-9.139c-0.385,0-0.702,0.313-0.706,0.698C10.523,10.499,10.836,10.819,11.226,10.825z"/>
				</svg><!-- /.usp-truck-icon -->
			</span>
			<span><strong>Free Delivery</strong> on Orders Over <strong>£30</strong></span>
	  	';
	[/ekm:element]

	[ekm:element]
	  	show='no';
	  	allowed_types='text, html';

	  	element_reference='usp-box-3';
	  	edit_button='YES';
	  	output_start='<div class="flex-display flex-col flex-justify-center usp-box usp-box-m usp-3">';
	  	output_end='</div><!-- /.flex-display .flex-col .flex-justify-center .usp-box .usp-box-m .usp-3 -->';
	  	element_name_default='USP BOX 3';
	  	output_default='
	  		<span>
				<svg version="1.1" class="usp-star-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					width="23.74px" height="22px" viewBox="0 0 23.74 22" enable-background="new 0 0 23.74 22" xml:space="preserve">
					<path d="M22.046,7.972l-6.141-1.223l-3.288-5.34c-0.148-0.237-0.4-0.389-0.679-0.407C11.915,0.999,11.893,1,11.87,1
					c-0.022,0-0.045-0.001-0.068,0.001c-0.279,0.019-0.531,0.17-0.679,0.407l-3.288,5.34L1.693,7.972c-0.47,0.098-0.771,0.56-0.672,1.03
					C1.055,9.168,1.138,9.32,1.258,9.439l4.484,4.484l-0.815,6.1c-0.059,0.476,0.279,0.911,0.756,0.97
					c0.149,0.018,0.3-0.002,0.439-0.059l5.747-2.459l5.747,2.459c0.139,0.057,0.29,0.078,0.439,0.059
					c0.476-0.059,0.815-0.493,0.756-0.97l-0.815-6.1l4.484-4.484c0.12-0.12,0.202-0.271,0.237-0.437
					C22.817,8.532,22.516,8.071,22.046,7.972z M16.476,12.999c-0.187,0.19-0.278,0.455-0.244,0.72l0.666,5.013l-4.687-2.011
					c-0.108-0.046-0.224-0.069-0.339-0.069s-0.231,0.023-0.339,0.069l-4.687,2.011l0.666-5.013c0.034-0.265-0.057-0.53-0.244-0.72
					L3.637,9.371l4.932-0.992C8.806,8.333,9.013,8.19,9.14,7.985l2.731-4.443l2.731,4.443c0.127,0.205,0.334,0.347,0.57,0.394
					l4.932,0.992L16.476,12.999z"/>
				</svg><!-- /.usp-star-icon -->
			</span>
			<span><strong>We Stock</strong> The Latest <strong>Big Brands!</strong></span>
	  	';
	[/ekm:element]
</section><!-- /.flex-grid .container-narrow .m-padding-left-m .m-padding-right-m .usp-bar -->


</header>







<main role="main" class="main-content-[ekm:page_type][/ekm:page_type]">
	[ekm:content]
</main><!-- /.main-content-[ekm:page_type][/ekm:page_type] -->


[ekm:showdata]
    data='recentlyviewedproducts';
    location='auto';

    cols='1';
    rows='6';
    page_navigation='no';

    orderby='viewdate';
    skiprecords='0';
    excludelist='';
    includelist='';

    editbuttons='YES';

    image_ratio='500';
    image_width='auto';
    image_height='auto';

    font_formatting='no';

    output_start='
    <section class="product-list rvp-products xl-padding-top-d xl-padding-bottom-d">
		[ekm:element]
			show='yes';
			allowed_types='text';

			element_reference='recently-viewed-products-title';
			edit_button='YES';
			output_start='<h2 class="h2-title section-title-border m-padding-bottom-d m-padding-bottom-m xl-margin-top-m"><span>';
			output_end='</span></h2><!-- /.section-title-border .m-padding-bottom-d .m-padding-bottom-m .xl-margin-top-m -->';
			element_name_default='Recently Viewed Products Title';
			output_default='Recently Viewed';
		[/ekm:element]

		<div class="flex-grid flex-wrap-wrap product-list rvp-product-list">
    ';
    output_item='
    		<div class="flex-col d-col-2 m-col-6 product-item">
				<div class="product-item-image">
					{image}[image]{/image}
				</div><!-- /.product-item-image -->

				{name}
					<div class="product-item-name m-margin-top-d m-margin-top-m s-padding-top-d s-padding-top-m">
						[name]
					</div><!-- /.product-item-name .m-margin-top-d .m-margin-top-m .s-padding-top-d .s-padding-top-m -->
				{/name}

			    <div class="flex-display flex-align-baseline price-star-wrapper m-margin-top-d m-margin-top-m">
				    {price}
						<div class="flex-display flex-align-baseline flex-wrap-wrap price-wrapper">
							<div class="product-item-price">[price]</div><!-- /.product-price -->
							{vat_price}<div class="product-item-vat">[vat_price] [vat_label]</div><!-- /.product-vat -->{/vat_price}
						</div><!-- /.flex-display .flex-align-baseline .price-wrapper -->
					{/price}

					{customer_reviews_average_stars}
						<div class="product-stars">
							[ekm:nested_if]
								iftype="EQUALS";
								ifvalue="[customer_reviews_total]";
								ifvalue2="";
								ifthen="";
								ifelse="
									<div class="product-review-stars product-review-stars-[customer_reviews_average_star_value]">
										<i class="fa fa-star" aria-hidden="true"></i><!-- /.fa .fa-star -->
										<i class="fa fa-star" aria-hidden="true"></i><!-- /.fa .fa-star -->
										<i class="fa fa-star" aria-hidden="true"></i><!-- /.fa .fa-star -->
										<i class="fa fa-star" aria-hidden="true"></i><!-- /.fa .fa-star -->
										<i class="fa fa-star" aria-hidden="true"></i><!-- /.fa .fa-star -->
										<span class="product-item-count">([customer_reviews_total])</span><!-- /.product-item-count -->
									</div><!-- /.product-review-stars product-review-stars-[customer_reviews_average_star_value] -->
								";
							[/ekm:nested_if]
						</div><!-- /.product-stars -->
					{/customer_reviews_average_stars}
			    </div><!-- /.flex-display .flex-align-baseline .price-star-wrapper .m-margin-top-d .m-margin-top-m -->
			</div><!-- /.flex-col .d-col-2 .m-col-6 .product-item -->
    ';
    including_vat_label='(inc. VAT)';
    excluding_vat_label='(ex. VAT)';
    output_end='
    	</div><!-- /.flex-grid .flex-wrap-wrap .product-list .rvp-product-list -->
    </section><!-- /.product-list .rvp-products .xl-padding-top-d .xl-padding-bottom-d -->
    ';
    output_column_blank='';
[/ekm:showdata]


<footer>
	[ekm:newsletter]
		output_start='<div class="flex-align-center newsletter-signup container l-padding-bottom-d l-padding-top-d l-padding-bottom-m l-padding-top-m">';
		output_item='
			<div class="flex-grid flex-align-center flex-justify-center newsletter-inner">
				[ekm:nested_element]
				show="yes";
				allowed_types="text";

				element_reference="newsletter-title";
				edit_button="YES";
				output_start="<h2 class="flex-col newsletter-title">";
				output_end="</h2><!-- /.flex-col .newsletter-title -->";
				element_name_default="Newsletter Title";
				output_default="Sign Up To Our Newsletter";
				[/ekm:nested_element]

				<div class="flex-col d-col-2 newsletter-copy">
					<div>Stay up to date on the latest product releases and offers by signing up to our newsletter.</div>
				</div><!-- /.flex-col .d-col-2 .newsletter-copy -->

				<div class="flex-display flex-col flex-wrap-wrap d-col-3 newsletter-form">
					[email]
					[signup_button]
					<div class="js-newsletterMessage"></div>
				</div><!-- /.flex-display .flex-col .flex-wrap-wrap .d-col-3 .newsletter-form -->
			</div><!-- /.flex-grid .flex-align-center .flex-justify-center .newsletter-inner -->
		';
		output_end='</div><!-- /.flex-align-center .newsletter-signup .container .l-padding-bottom-d .l-padding-top-d .l-padding-bottom-m .l-padding-top-m -->';
		email_textbox_attribute='placeholder="Example: example@email.com"';
		signupbutton_image='/ekmps/designs/assets/master/1549/images/newsletter-arrow.svg';
		on_submit='newsletterSubmit';
	[/ekm:newsletter]

	<script>
		//Newsletter Form Message
		function newsletterSubmit( email, firstname, lastname ) {
			var element = document.querySelector(".js-newsletterMessage"),
				message = "You have been successfully subscribed";
		    if ( !(/.+@.+\..+/).test(email) ) {
		    	message = "Please enter a valid email address";
		    }
			element.textContent = message;
		}
	</script>


	<section class="container footer-columns l-padding-bottom-d l-padding-top-d l-padding-bottom-m l-padding-top-m">
		<div class="flex-grid flex-align-center">
			<div class="flex-col d-col-6 m-col-12 footer-column-left">
				<div class="copyright">
					[ekm:element]
						element_reference='copyright';
						edit_button='YES';
						output_start='';
						output_end='';
						element_name_default='copyright';
						output_default='
							[ekm:nested_if]
								iftype="EQUALS";
								ifvalue="[ekm:shop_contact_details]output_item='{copyright}[copyright]{/copyright}';[/ekm:shop_contact_details]";
								ifvalue2="";
								ifthen="&copy; [ekm:sitename][/ekm:sitename]. All Rights Reserved.";
								ifelse="
									[ekm:shop_contact_details]
										output_start='<span class="ekmps-copyright">';
										output_item='
											{copyright}
												[copyright]
											{/copyright}
										';
										output_end='</span><!-- /.ekmps-copyright -->';
									[/ekm:shop_contact_details]
								";
							[/ekm:nested_if]
						';
					[/ekm:element]

					[ekm:poweredby]
						output_start='&nbsp;/&nbsp;&nbsp;';
						output_end='';
					[/ekm:poweredby]
				</div><!-- /.copyright -->

				[ekm:social]
				   	name='default';
				    output_start='<div class="social-icons l-margin-top-d xl-margin-top-m">';
				    widget_start='<div>';
				    widget_end='</div>';
					output_end='</div><!-- /.social-icons .l-margin-top-d .xl-margin-top-m -->';
				[/ekm:social]
			</div><!-- /.flex-col .d-col-6 .m-col-12 .footer-column-left -->


			<div class="flex-col d-col-6 m-col-12 footer-column-right">
				<div class="webpages xl-margin-top-m">
					[ekm:show_webpages]
					    orderby='auto';
					    counterstart='auto';
					    groups='auto';
					    counterreset='auto';
					    output_start='
							<ul class="flex-display flex-wrap-wrap ul-reset webpages-list">
						    	<li><a href="index.asp">Home</a></li>
					    ';
					    output_item='<li>[link]</li>';
					    output_item_alternate='';
					    output_item_current='';
					    output_item_blank='';
					    output_end='
								<li><a href="terms.asp">Terms</a></li>
								<li><a href="sitemap.asp">Sitemap</a></li>
								<li><a href="privacy.asp">Privacy</a></li>
							</ul><!-- /.display-flex .flex-wrap-wrap .ul-reset .webpages-list -->
						';
					[/ekm:show_webpages]
				</div><!-- /.webpages .xl-margin-top-m -->

				[ekm:card_logos]
					output_start='<div class="card-logos l-margin-top-d xl-margin-top-m">';
					output_end='</div><!-- /.card-logos .l-margin-top-d .xl-margin-top-m -->';
				[/ekm:card_logos]
			</div><!-- /.flex-col .d-col-6 .m-col-12 .footer-column-right -->
		</div><!-- /.flex-grid .flex-align-center -->


		[ekm:shop_contact_details]
			output_start='<div class="ekmps-vat-number-company-number l-margin-top-d xl-margin-top-m">';
			output_item='
				{vat_number}
					<span class="ekmps-vat-number">VAT Number: [vat_number]</span><!-- /.ekmps-vat-number -->
				{/vat_number}

				{company_number}
					<span class="ekmps-company-number">- Registered company: [company_number]</span><!-- /.ekmps-company-number -->
				{/company_number}
			';
			output_end='</div><!-- /.ekmps-vat-number-company-number .l-margin-top-d .xl-margin-top-m -->';
		[/ekm:shop_contact_details]


		[ekm:element]
		  	show='yes';
		  	allowed_types='text';

		  	element_reference='contact-details';
		  	edit_button='YES';
		  	output_start='<div class="address-element s-margin-top-d xl-margin-top-m">';
		  	output_end='</div><!-- /.address-element s-margin-top-d xl-margin-top-m -->';
		  	element_name_default='Address Element';
		  	output_default='Your Store Name, Your Address, Your Town, Your County, Your Country, AB1 2CD - 01234 567 890';
		[/ekm:element]


		[ekm:multicurrency]
			output_start='<div class="currency s-margin-top-d xl-margin-top-m">';
			displaytype='dropdown';
			output_end='</div><!-- /.currency .s-margin-top-d xl-margin-top-m -->';
		[/ekm:multicurrency]
	</section><!-- .container .footer-columns .l-padding-bottom-d .l-padding-top-d .l-padding-bottom-m .l-padding-top-m -->
</footer>



</body><!-- /.push-drawer -->



<script src="/ekmps/designs/assets/master/1549/other/script.js"></script>



</html>


