<div id="help-page">
    <div class="container">
        <div class="row">
            <div class="panel-container" id="breadcrumb-div">
                <a href="/">Home</a> > 
                <a href="index.asp?function=search" id="active">Search</a> - 
                <span style="display:inline-block;">
        [ekm:search]</span> 

        image_ratio='150';  
        cols='2';  
        rows='10';
        
        search_form_content='
        
        <h2 style="margin-top:0;padding-top:0;text-transform:capitalize;" class="orange">[search-term]</h2>
            </div>
        </div>';
        
        output_start='
            <div class="row">
            <div id="side-bar">
                <ul class="side-nav" id="search">
                    ';
                    output_product_item='
                    <li><a href='#[id]'>
                            [name]
                        </a></li>
                    ';
        output_end='
                </ul>       
            </div>
                ';
                
                output_no_results=''; 
                
        [/ekm:search]
        [ekm:search]
                image_ratio='150';  
        cols='2';  
        rows='10';
                output_start='
                <br/>
            <div id="main-column" class="searchCol">
                <div class="search-nav">[next-back-buttons]</div>
                <hr/>
                ';
	  

        output_product_item='
            <a name="[id]"></a>
                <div class="search-container">
                    <div class="image-container">
                        [image]
                    </div>
                    <div class="content-container" style="display:inline-block;">
                        <span class="title">[name]</span>
                        <span class="price">[price]</span>
                        <a href="-[id]-p.asp"><div class="button">BUY NOW</div></a>
                    </div>
                </div>
                <hr/>
                ';
                
        output_end='
            <div class="search-nav">[next-back-buttons]</div>        
        </div>';    

        output_next_back='[back-button][number-of-pages-list][next-button]';  

        output_no_results='
            <div id="no-search-results">
                <p>has returned no results.</p>
            </div>';   

    [/ekm:search]</div>

    </div>
</div>


<script>

    
    $(document).ready(function()
    {
        $.each($('.product'), function(index, elem) {

            var link = $(elem).find('a').eq(0).attr('href');

            $(elem).wrapInner('<a href="' + link + '"/>');
        });

    });
    
</script>