<div id="search-page">
	<form action="index.asp?function=SEARCH" method="post">
		<table width="100%" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td>
					<font size="4"><b>Search</b></font>
					<br /><br />
					<font size="2">To search for a product enter some search words below…<br /><br /></font>
				</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>Search Term</td>
				<td><input name="search" TYPE="text" value="sticky" size="25"></td>
			</tr>
			<tr>
				<td colspan="2"><hr /></td>
			</tr>
			<tr>
				<td>Include Product Name</td>
				<td><input name="pn" type="checkbox" value="1" checked/></td>
			</tr>
			<tr>
				<td>Include Product Brand</td>
				<td><input name="brand" type="checkbox" value="1" [search-product-brand]/></td>
			</tr>
			<tr>
				<td>Include Product Full Descriptions</td>
				<td><input name="pd" type="checkbox" value="1" /></td>
			</tr>
			<tr>
				<td>Include Product Short Descriptions</td>
				<td><input name="pds" type="checkbox" value="1" /></td>
			</tr>
			<tr>
				<td colspan="2"><hr /></td>
			</tr>
			<tr>
				<td>Include Product Code</td>
				<td><input name="pc" type="checkbox" value="1" /></td>
			</tr>
			<tr>
				<td>Include Product Global Trade Item Number (GTIN)</td>
				<td><input name="gtin" type="checkbox" value="1" /></td>
			</tr>
			<tr>
				<td>Include Product Manufacturer Part Number (MPN)</td>
				<td><input name="mpn" type="checkbox" value="1" /></td>
			</tr>
			<tr>
				<td colspan="2"><hr /></td>
			</tr>
			<tr>
				<td>Include Categories</td>
				<td><input name="ic" type="checkbox" value="1" /></td>
			</tr>
			<tr>
				<td>Include Categories Descriptions</td>
				<td><input name="icd" type="checkbox" value="1" /></td>
			</tr>
			<tr>
				<td>Include In Categories Description</td>
				<td><input name="iicd" type="checkbox" value="1" /></td>
			</tr>
			<tr>
				<td colspan="2"><hr /></td>
			</tr>
			<tr>
				<td><font size="3"><b>Limit Results by Price</b></font></td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>Minimum Price</td>
				<td><input name="minPrice" type="text" value="" size="10"></td>
			</tr>
			<tr>
				<td>Maximum Price</td>
				<td><input name="maxPrice" type="text" value="" size="10"></td>
			</tr>
			<tr>
				<td colspan="2"><hr /></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><input type="submit" value="Search"></td>
			</tr>
		</table>
	</form>
	<hr size="1" width="100%">
	<table border="0">
		<tr>
			<td>
				<font size="3" face="arial"><strong>Search Results</strong><br /></font>
				<br />
				<strong>Pages:</strong> 1 of 1<br />
				<strong>Displaying results:</strong> 1 to 2<br /> <strong>Total number of results:</strong> 2<br />
				<br />
			</td>
		</tr>
	</table>
	<table border="0" cellspacing="5">
		<tr>
			<td width="5"></td>
			<td>
				<span ID="toggleopacity32371">
					<a  href="warmup--200w-sticky-mat-kit-32371-p.asp">
						<img itemprop="image" src="/ekmps/shops/plumbmein/images/warmup-200w-sticky-mat-kit-32371-p[ekm]70x63[ekm].jpg?_=750924318" width="70" height="63" alt="Warmup  200w Sticky Mat kit" id="_EKM_SEARCHIMAGE" border="0" />
					</a>
				</span>
			</td>
			<td>
				<font size="3" face="arial">
					<a href="warmup--200w-sticky-mat-kit-32371-p.asp">Warmup  200w Sticky Mat kit</a>
				</font>
				<br />
				<font size="2" color="DARKRED" face="arial">£24.99</font>
				<br />
			</td>
		</tr>
		<tr>
			<td width="5"></td>
			<td>
				<span ID="toggleopacity33151">
					<a  href="warmup-150w-sticky-mat-kit-33151-p.asp">
						<img itemprop="image" src="/ekmps/shops/plumbmein/images/warmup-150w-sticky-mat-kit-33151-p[ekm]70x63[ekm].jpg?_=850924318" width="70" height="63" alt="Warmup 150w Sticky Mat kit" id="_EKM_SEARCHIMAGE" border="0" />
					</a>
				</span>
			</td>
			<td>
				<font size="3" face="arial">
					<a href="warmup-150w-sticky-mat-kit-33151-p.asp">Warmup 150w Sticky Mat kit</a>
				</font>
				<br />
				<font size="2" color="DARKRED" face="arial">£49.16</font>
				<br />
			</td>
		</tr>
	</table>
</div>