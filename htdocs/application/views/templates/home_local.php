<div id="slideshow" class="flexslider">
	<div class="slides">
		<div class="slide">
			<a href="16mm-pipe-26427-p.asp">
                <img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/design/hp-banner-pipe.png" alt="High quality multi layered pipe">
			</a>
		</div>
		<div class="slide">
			<a href="underfloor-heating-mats-362-c.asp">
                <img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/design/hp-banner-electric-mats.png" alt="Underfloor Heating Electric Mat Kits">
			</a>
		</div>
		<div class="slide">
			<a href="water-underfloor-heating-359-c.asp">
                <img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/design/hp-banner-water-kits.png" alt="Underfloor Heating Water Kits">
			</a>
		</div>
		<div class="slide">
			<a href="tile-backer-boards-26373-p.asp">
                <img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/design/hp-banner-backer-boards.png" alt="Underfloor Heating Backer Boards">
			</a>
		</div>
	</div>
</div>
<div id="slide-controls">
	<div class="control">
		<span class="directicon directicon-contained-circles"></span>
		<span class="words">Multi<br /> Layered Pipe</span>
	</div>
	<div class="control">
		<span class="directicon directicon-contained-electric-system"></span>
		<span class="words">Electric<br /> Mat Kits</span>
	</div>
	<div class="control">
		<span class="directicon directicon-contained-water-system"></span>
		<span class="words">Complete<br /> Water Kits</span>
	</div>
	<div class="control">
		<span class="directicon directicon-contained-layers"></span>
		<span class="words">Backer<br /> Boards</span>
	</div>
</div>


<div id="homepage-hero-panels">
    <div class="product-panel">
        <a href="electric-underfloor-heating-355-c.asp">
            <h2>
            	<span class="with-icon electric"></span>
            	<span>Electric Underfloor Heating</span>
            </h2>
            <div class="image-block">
                <img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/design/main-electric-picture.jpg"/>
            </div>
            <div class="button-block">
                <div class="price-block">
                    <span class="from">kits from just</span>
                    <span class="price-value">
                        <strong>67.99</strong>
                        <div>
                            <span class="pence"></span>
                            <span class="vat">inc vat</span>
                        </div>
                    </span>
                </div>
                <div class="button">SHOP NOW</div>
                <img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/design/varm-haus.png" class="ufhd-logo">
            </div>
        </a>
    </div>
    <div class="product-panel">
        <a href="water-underfloor-heating-359-c.asp">
            <h2>
            	<span class="with-icon water"></span>
            	<span>Water Underfloor Heating</span>
            </h2>
            <div class="image-block">
                <img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/design/main-water-picture.jpg"/>
            </div>
            <div class="button-block">
                <div class="price-block">
                    <span class="from">kits from just</span>
                    <span class="price-value">
                        <strong>307.99</strong>
                        <div>
                            <span class="pence"></span>
                            <span class="vat">inc vat</span>
                        </div>
                    </span>
                </div>
                <div class="button">SHOP NOW</div>
                <img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/design/varm-haus.png" class="ufhd-logo">
            </div>
        </a>
    </div>
</div>

<div class="homepage-sub-product-panels">
        <ul data-height-determined-by=".image-container">
            <li>
                <a href="/pipes-382-c.asp">
                    <h2>Pipes</h2>
                    <div class="image-container"><img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/design/homepage-accessories-pipe.jpg"/></div>
                    <p>from <strong>£27.49</strong> inc vat</p>
                    <span class="button filled">buy now</span>
                </a>
            </li>
            <li>
                <a href="/manifolds-380-c.asp">
                    <h2>Manifolds</h2>
                    <div class="image-container"><img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/design/homepage-accessories-manifold.jpg"/></div>
                    <p>from <strong>£2.38</strong> inc vat</p>
                    <span class="button filled">buy now</span>
                </a>
            </li>
            <li>
                <a href="/fixing-systems-379-c.asp">
                    <h2>Spreader Plates</h2>
                    <div class="image-container"><img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/design/homepage-accessories-spreader-plates.jpg"/></div>
                    <p>from <strong>£19.99</strong> inc vat</p>
                    <span class="button filled">buy now</span>
                </a>
            </li>
            <li>
                <a href="/tile-adhesive-and-levellers-407-c.asp">
                    <h2>Tile Adhesives</h2>
                    <div class="image-container"><img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/design/homepage-accessories-adhesive.jpg"/></div>
                    <p>from <strong>£38.99</strong> inc vat</p>
                    <span class="button filled">buy now</span>
                </a>
            </li>
        </ul>
</div>

<div class="homepage-sub-product-panels">
        <ul data-height-determined-by=".image-container">
            <li>
                <a href="/electric-414-c.asp">
                    <h2>Thermostats <br/>for electric systems</h2>
                    <div class="image-container"><img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/design/homepage-accessories-electric-thermostat.jpg"/></div>
                    <p>from <strong>£27.49</strong> inc vat</p>
                    <span class="button filled">buy now</span>
                </a>
            </li>
            <li>
                <a href="/electric-408-c.asp">
                    <h2>Insulation boards <br/>for electric systems</h2>
                    <div class="image-container"><img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/design/homepage-accessories-electric-board.jpg"/></div>
                    <p>from <strong>£2.38</strong> inc vat</p>
                    <span class="button filled">buy now</span>
                </a>
            </li>
            <li>
                <a href="/water-415-c.asp">
                    <h2>Thermostats <br/>for water systems</h2>
                    <div class="image-container"><img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/design/homepage-accessories-water-thermostat.jpg"/></div>
                    <p>from <strong>£19.99</strong> inc vat</p>
                    <span class="button filled">buy now</span>
                </a>
            </li>
            <li>
                <a href="/water-409-c.asp">
                    <h2>Insulation boards <br/>for water systems</h2>
                    <div class="image-container"><img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/design/homepage-accessories-water-board.jpg"/></div>
                    <p>from <strong>£38.99</strong> inc vat</p>
                    <span class="button filled">buy now</span>
                </a>
            </li>
        </ul>
</div>

<div id="link-panels" data-height-determined-by=".description">
	<div class="link-panel confused primary">
		<a href="confused-224-w.asp">
			<h3><span></span>Confused?</h3>
			<img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/design/panel-image-girl-thinking.jpg">
			<div class="copy-container">
				<div class="description">
					<p>There are 2 main types of Underfloor heating, electric & water, both do the same thing, warm the floor and the room.</p>
				</div>
				<div class="button brand">Learn More</div>
			</div>
		</a>
	</div>
	<div class="link-panel measuring-up secondary">
		<a href="how-to-measure-up-236-w.asp">
			<h3><span></span>Measuring up</h3>
			<img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/design/panel-image-tape-measure.jpg">
			<div class="copy-container">
				<div class="description">
					<p>Why not take a look at our handy how-to guides and discover the best way of measuring the floor space you need to heat.</p>
				</div>
				<div class="button brand">Learn More</div>
			</div>
		</a>
	</div>
	<div class="link-panel how-to-install primary">
		<a href="how-to-install-235-w.asp">
			<h3><span></span>How to install</h3>
			<img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/design/panel-image-man-with-pipe.jpg">
			<div class="copy-container">
				<div class="description">
					<p>Need a little help with your installation?  We have provided install manuals for all our products to make life easier.</p>
				</div>
				<div class="button brand">Learn More</div>
			</div>
		</a>
	</div>
	<div class="link-panel request-a-quote brand">
		<a href="request-a-quote-239-w.asp">
			<h3><span></span>Request a quote</h3>
			<img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/design/panel-image-people-planning.jpg">
			<div class="copy-container">
				<div class="description">
					<p>Do you need a price for your next project? Send us your architectural drawings and we'll provide you a quote straight away.</p>
				</div>
				<div class="button">Get quote</div>
			</div>
		</a>
	</div>
</div>