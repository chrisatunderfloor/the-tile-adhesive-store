<div class="row" id="breadcrumb">
	<nav>[ekm:location][/ekm:location]</nav>
</div>

<div id="product-details">

	[ekm:productstart][/ekm:productstart]

	<div class="image-container">
		[ekm:productimage]
		width='750';
		opentype='popup';
		extra_images='popup';
		images_layout='STYLE1';
		extra_images_width='auto';
		extra_images_height='auto';
		[/ekm:productimage]
	</div>

	<div class="options-container">

		<h1><span itemprop="name">[ekm:productname]font_formatting='no';[/ekm:productname]</span></h1>

		<!-- PRODUCT OPTIONS -->
		[ekm:productoptions]
		font_formatting='no';
		remove_formatting='yes';
		orderby='name ASC';
		orderbyitems='auto';

		template_start='<div class="option-container">';
			dropdown_content='<div><label>[option_title]</label>[option_item]</div>';
		template_end='</div>';
		[/ekm:productoptions]

		<!--  ADD TO CART BUTTON -->
		<div class="button-holder">
			<div id="qty-field-container">
				<label>Qty</label>
				[ekm:productqty]
				output_start='<div class="c-product-add-to-cart__quantity">';
					output_item='[input]';
				output_end='</div>';
				[/ekm:productqty]
			</div>
			<div id="add-to-cart-button-container">
				[ekm:addtocart][/ekm:addtocart]
			</div>
			<div id="out-of-stock">
				We are currently out of stock on that item.
			</div>
		</div>

		[ekm:if]
		iftype='GREATERTHAN';
		ifvalue='[ekm:productrrp]rrp_text_layout='[rrp]';value_only='yes';[/ekm:productrrp]';
		ifvalue2='0';
		ifthen='
		<p class="rrp">RRP:
			<strong>
				[ekm:productrrp]
				rrp_text_layout='[rrp]';
				line='no';
				rrp_font='auto';
				rrp_colour='#FF6500';
				[/ekm:productrrp]
			</strong>
		</p>
		';
		ifelse='';
		[/ekm:if]

		<p class="price">
			Our Price:
			<strong>[ekm:productprice]price_text_layout='[price]';[/ekm:productprice]</strong>
			<span class="vat">inc VAT</span>
		</p>

		<!-- TrustBox widget - Micro Combo -->
		<div class="trustpilot-widget" data-locale="en-GB" data-template-id="5419b6ffb0d04a076446a9af" data-businessunit-id="4eca53ef0000640005119918" data-style-height="20px" data-style-width="100%" data-theme="light">
			<a href="https://uk.trustpilot.com/review/www.underfloorheating-direct.com" target="_blank">Trustpilot</a>
		</div>
		<!-- End TrustBox widget -->

	</div>
</div>

[ekm:productend][/ekm:productend]

[ekm:showdata]
data='relatedproducts';
location='auto';
orderby='price DESC';
cols='1';
rows='50';
page_navigation='NO';
font_formatting='no';
output_start='
<section id="related-products" class="slider">
	<h1>Related Products</h1>
	<div class="slide-container">
		<div class="slider related-products-slider" data-height-determined-by=".image-container,p">
			';
			output_row_start='';
			output_column_start='';
			output_item='
			<div>
				<a href="[url]" target="_blank" class="related-product">
					<div class="image-container">
						<img src="[image-url]">
					</div>
					<p>[name-NOEDIT-NOLINK]</p>
					<span>[price]</span>
				</a>
			</div>
			';
			output_column_end='';
			output_row_end='';
			output_end='
		</div>
	</div>
</section>';
[/ekm:showdata]


<!--------------->
<!--Tab Content-->
<!--------------->
<div id="tabbed-content-container">
	<dl id="tab-content">
		<!--Details Content-->
		<dt class="product-details" data-tab="details">Product Details</dt>
		<dd data-tab="details">
			<h2>Product Details</h2>
			[ekm:productdescription] font_formatting='no'; [/ekm:productdescription]
		</dd>

		<!--Installation Content-->
		[ekm:if]
		iftype='ELEMENT_NOT_EMPTY';
		ifvalue='Tab-2[ekm:pageid]idonly="no";[/ekm:pageid]';
		ifthen='<dt data-tab="installation" class="installation">Installation</dt>';
		ifelse='';
		[/ekm:if]
		<dd data-tab="installation">
			[ekm:if]
			iftype='ELEMENT_NOT_EMPTY';
			ifvalue='Tab-2[ekm:pageid]idonly="no";[/ekm:pageid]';
			ifthen='
			<h2>Installation</h2>
			[ekm:nested_element]elementreference="Tab-2[ekm:pageid]idonly="no";[/ekm:pageid]";edit_button="YES";[/ekm:nested_element]';
			ifelse='';
			[/ekm:if]
		</dd>

		<!--Specification Content-->
		[ekm:if]
		iftype='ELEMENT_NOT_EMPTY';
		ifvalue='Tab-3[ekm:pageid]idonly="no";[/ekm:pageid]';
		ifthen='<dt data-tab="specification" class="specification">Specification</dt>';
		ifelse='';
		[/ekm:if]
		<dd data-tab="specification">
			[ekm:if]
			iftype='ELEMENT_NOT_EMPTY';
			ifvalue='Tab-3[ekm:pageid]idonly="no";[/ekm:pageid]';
			ifthen='
			<h2>Specification</h2>
			[ekm:nested_element]elementreference="Tab-3[ekm:pageid]idonly="no";[/ekm:pageid]";edit_button="YES";[/ekm:nested_element]';
			ifelse='';
			[/ekm:if]
		</dd>

		<!--Thermostat Content-->
		[ekm:if]
		iftype='IN';
		ifvalue='[ekm:categoryid][/ekm:categoryid]';
		ifvalue2='355,362,366,367,368,369,359,373,374,375,376,377';
		ifthen='<dt data-tab="thermostats" class="thermostats">Thermostats</dt>';
		ifelse='';
		[/ekm:if]

		<dd data-tab="thermostats">
			[ekm:if]
			iftype='IN';
			ifvalue='[ekm:categoryid][/ekm:categoryid]';
			ifvalue2='355,362,366,367,368,369';
			ifthen='

			<!-- **** thermostats for ELECTRIC kits **** -->
			<h2>Thermostats</h2>
			<div class="section">
				<div class="content-container">
					<p>All our kits come with a Manual Dial Thermostat for electric floor heating. Three modes of operation are available on this model and a 3 meter floor probe is supplied.</p>
					<h6>Technical Info</h6>
					<ul class="tick">
						<li>Air Sensor, Floor Sensor and & Air & Floor Modes are available</li>
						<li>13A Rating</li>
						<li>Heat and Power On indication</li>
						<li>Modern Appearance</li>
						<li>Easily Increase or Decrease the Required Temperature</li>
						<li>Variable Switching Differential</li>
						<li>Range: 05-35&deg;C</li>
					</ul>
					<p><strong>Important Note:</strong> This thermostat is an electronic dial thermostat, offering greater control when compared with standard bi-metallic thermostats. This thermostat does need at least 3 wires to operate (Live, Neutral and Switch wire) Please check your wiring and the manual download to ensure your system is compatible.</p>
				</div>
				<div class="image-container right">
					<img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/design/electric-manual-thermostat.jpg">
				</div>
			</div>
			<div class="section">
				<div class="content-container">
					<h3>Digital Thermostat Upgrade</h3>
					<p>Featuring a blue back light operation and can switch up to 3600 watts, this thermostat has the ability to be switched from either floor sensor, air sensor or a combination of both floor and air sensors internally, this unit is available in a white finish.</p>
					<h3>Technical Info</h3>
					<ul class="tick">
						<li>Stylish and slimline design</li>
						<li>Blue back light illumination</li>
						<li>Switches upto 16 amps (3500 watts) 230v AC</li>
						<li>Can be either an air only / floor only / Air & Floor 7 day programmable (each day can be set different)</li>
						<li>4 set points per day (2 comfort - 2 economy)</li>
						<li>Manual overide and vacation mode</li>
						<li>3 meter remote floor probe included</li>
					</ul>
					<table class="table side">
						<tr>
							<th>Dimensions</th>
							<td>87mm x 87mm x 13mm</td>
						</tr>
						<tr>
							<th>Mounting</th>
							<td>Flush mounted 35mm deep galvanished box (single socket)</td>
						</tr>
						<tr>
							<th>Load</th>
							<td>3500w - 230v - 50Hz</td>
						</tr>
					</table>
				</div>
				<div class="image-container">
					<img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/design/prod-electric-digital-thermostat.jpg">
				</div>
			</div>
			';
			ifelse='';
			[/ekm:if]

			[ekm:if]
			iftype='IN';
			ifvalue='[ekm:categoryid][/ekm:categoryid]';
			ifvalue2='359,373,374,375,376,377';
			ifthen='

			<!-- **** thermostats for WATER kits **** -->
			<h2>Thermostats</h2>
			<div class="section">
				<div class="content-container">
					<p>All our kits come with the Heatmiser DS1, which is our 230v electronic room thermostat, ideal for conventional or combi-boiler systems. This thermostat is used in conjunction with a separate programmer.</p>
					<h6>Technical Info</h6>
					<ul class="tick">
						<li>Power On & Heat On Indication</li>
						<li>Modern Apperance</li>
						<li>Easily increase or decrease the required temperature</li>
						<li>Variable Switching Differential</li>
						<li>Outputs: Volt free 3A Max</li>
					</ul>
					<table class="table side">
						<tr>
							<th>Dimensions</th>
							<td>92mm x 92mm x 34mm</td>
						</tr>
						<tr>
							<th>Mounting</th>
							<td>Surface mounting</td>
						</tr>
						<tr>
							<th>Load</th>
							<td>230v</td>
						</tr>
					</table>
				</div>
				<div class="image-container">
					<img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/design/electric-manual-thermostat.jpg">
				</div>
			</div>
			<div class="section">
				<div class="content-container">
					<h2>Digital Thermostat Upgrade</h2>
					<p>The Heatmiser PRT is our 12v programmable room stat and programmer in one, providing up to 4 different temperatures at different times of the day.</p>
					<h3>Technical Info</h3>
					<ul class="tick">
						<li>Large display</li>
						<li>Facility to limit use of Up/Down Temp Keys</li>
						<li>Automatic blue back-light (Turns off after 30 seconds)</li>
						<li>Heatmiser TouchPad, Netmonitor Compatible</li>
						<li>&deg;C / &deg;F Selectable</li>
						<li>Key Locking Facility</li>
						<li>Frost protection with adjustable temperature setting</li>
						<li>Easily increase or decrease the required temperature</li>
						<li>Remote control optional</li>
					</ul>
					<table class="table side">
						<tr>
							<th>Dimensions</th>
							<td>98mm x 85mm x 13mm</td>
						</tr>
						<tr>
							<th>Mounting</th>
							<td>Flush mounted</td>
						</tr>
						<tr>
							<th>Load</th>
							<td>230v</td>
						</tr>
					</table>
				</div>
				<div class="image-container">
					<img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/design/prod-water-digital-thermostat.jpg">
				</div>
			</div>
			';
			ifelse='';
			[/ekm:if]
		</dd>

		<!--Delivery Content-->
		<dt data-tab="delivery" class="delivery">Delivery</dt>
		<dd data-tab="delivery">
			<h2>Delivery</h2>
			<p>Our delivery charges are calculated based on the weight of your entire order.  Below is a table illustrating the cost for each weight bracket, please note we cap our delivery costs to a maximum of &pound;50.</p>
			<table class="top">
				<tr>
					<th>Weight From (kg)</th>
					<th>Weight To (kg)</th>
					<th>Cost</th>
				</tr>
				<tr>
					<td>0</td>
					<td>39.99</td>
					<td>&pound;10</td>
				</tr>
				<tr>
					<td>40</td>
					<td>59.99</td>
					<td>&pound;20</td>
				</tr>
				<tr>
					<td>60</td>
					<td>79.99</td>
					<td>&pound;30</td>
				</tr>
				<tr>
					<td>80</td>
					<td>99.99</td>
					<td>&pound;40</td>
				</tr>
				<tr>
					<td>100+</td>
					<td>&nbsp;</td>
					<td>&pound;50</td>
				</tr>
			</table>

			<p>In addition to our standard delivery service, we can also arrange for delivery on a Saturday or before 10.30am on any working day. This level of carriage comes at an additional cost:</p>
			<ul class="tick">
				<li>Next working day before 10.30am - <span class="highlight">Additional £5</span></li>
				<li>Saturday - <span class="highlight">Additional £20</span></li>
			</ul>
			<p>We can also arrange for pallet delivery on a three-day delivery, but costs vary depending on the size of the order, so please contact us for details.</p>

			<h2>When will I receive my order?</h2>
			<p>Orders placed before 3pm on any business day (Monday – Friday) excluding Bank Holidays should be delivered on the next business day (Excluding Saturday, unless the carriage is met) anytime up to 7.00pm.</p>
			<p>Our standard next working day delivery does not guarantee next day delivery; however every effort is made by us and the courier to achieve this goal.</p>
			<p>Orders taken on Friday after 3pm, Saturday or Sunday will be dispatched on Monday (excluding Bank Holidays) for delivery on Tuesday up to 7.00pm.</p>
			<p>Orders placed on Friday before 3pm will be dispatched on Friday for delivery on Monday up to 7.00pm unless the Saturday delivery charge is met (This excludes Bank Holidays).</p>
			<p><span class="note">Please note:</span> We rely upon the services of third party couriers and, whilst we endeavor to always deliver in a timely manner, there are unfortunately a few occasions when the couriers fail to deliver on the appointed day. This happens rarely, but because it does occur, albeit occasionally, we cannot be held responsible for the late delivery of an item, or any costs incurred by customers tradesman unable to work, please ensure you have your materials before booking in any trades.</p>
			<p>Shipments sent outside the European Union may be subject to import duties and taxes, which may be levied once a shipment reaches your country. For details of Customs charges please consult your own Customs authorities for the latest rates.</p>
			<p>Additional charges for customs clearance must be borne by you; unfortunately we have no control over these charges and cannot predict what they may be. We are unable to reimburse any costs incurred regardless of the circumstances.</p>
		</dd>
	</dl>
</div>