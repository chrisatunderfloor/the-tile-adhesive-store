<div class="row" id="breadcrumb">
	<nav>[ekm:location][/ekm:location]</nav>
</div>

[ekm:incategorydescription][/ekm:incategorydescription]

<div id="main-column" class="category-page">

	<!-- Output product details -->
    [ekm:showdata]
        data='product';
        location='auto';
        cols='2';
        rows='100';
        counterreset='1';
        page_navigation='no';
        orderby="price ASC, name ASC";
        skiprecords='0';
        editbuttons='YES';
        image_ratio='180';
        image_width='500';
        font_formatting='no';
        output_start='
	<div class="category-panel-container">
		<div data-height-determined-by="h2">';
        output_row_start='';
        output_column_start='';
        output_item='
	        <div class="product-panel">
	            <a href="[url]">
	                <h2>[name-NOEDIT-NOLINK]</h2>
	                <div class="image-block">
	                    <img src="[image-url]">
	                </div>
	                <div class="button-block">
	                    <div class="price-block">
	                    	<span class="from">
								<!--
									25740 - Underfloor heating 100w mat kit
									25828 - Underfloor heating 150w mat kit
									25913 - Underfloor heating 200w mat kit
									32058 - Devi mat 100w
									32073 - Devi mat 150w
									32088 - Devi mat 200w
									25980 - Underwood heating kit
									25166 - 100w Loose Cable Kit
									25254 - 150w Loose Cable Kit
									25342 - 200w Loose Cable Kit
									25430 - Under carpet heating kit
									25538 - In Screed heating kit
									32105 - John Guest Standard Output Pack
									32122 - John Guest Mulit Room Pack
									32238 - Varm Haus High Output Kit
								 -->
	                    		[ekm:nested_if]
				                    iftype="IN";
				                    ifvalue="[id]";
				                    ifvalue2="25740,25828,25913,32058,32073,32088,25980,25166,25254,25342,25430,25538,32105,32122,32238";
				                    ifthen="from only";
				                    ifelse="only";
			                    [/ekm:nested_if]
	                    	</span>
	                    	<span class="price-value">
	                    		<strong>[price]</strong>
	                    		<div>
				                    <span class="pence"></span>
				                    <span class="vat">inc vat</span>
				                </div>
				            </span>
				        </div>
	                    <div class="button">SHOP NOW</div>
	                    [ekm:nested_if]
		                    iftype="EQUALS";
		                    ifvalue="[brand]";
		                    ifvalue2="Heatmiser";
		                    ifthen="<img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/design/heatmiser-logo.png" class="ufhd-logo">";
		                    ifelse="<img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/design/ufhd.png" class="ufhd-logo">";
	                    [/ekm:nested_if]
	                </div>
	            </a>
	        </div>
	        [editbuttons]';
        output_column_end='';
        output_row_end='';
        output_end='
        </div>
	</div>';
        output_column_blank='';
    [/ekm:showdata]

	<!-- Hero categories on electric and water pages -->
    [ekm:showdata]
        data='categories';
        location='auto';
        includelist='362,367,373,374,375,434';
        excludelist='';
        cols='1';
        rows='100';
        counterreset='1';
        page_navigation='no';
        orderby='auto';
        skiprecords='0';
        editbuttons='YES';
        image_ratio='180';
        image_width='500';
        font_formatting='no';
        output_start='
		    <div class="category-panel-container">
				<div class="hero-categories" data-height-determined-by=".image-container,.description h3,.description">';
        output_row_start='';
        output_item='
			       	<div class="category-panel">
			            <div class="image-container">
			                <a href="[url]">
			                    <img src="[image-url]" />
			                </a>
			            </div>
			            <div class="content-container">
			                <div class="description">
			                    <h3>[name-NOEDIT-NOLINK]</h3>
			                    [shortdescription]
			                </div>
			                <div class="price-container">
			                    <div class="price-block">
			                    	<span class="from">kits from just</span>
			                    	<span class="price-value">
			                    		<strong>
			                    			<!-- Output cheapest product price -->
									        [ekm:nested_nested_showdata]
										        data=(product);
										        location=([id]);
										        cols=(1);
										        rows=(1);
										        counterreset=(1);
										        page_navigation=(no);
										        orderby=(price ASC);
										        skiprecords=(0);
										        editbuttons=(NO);
										        image_ratio=(180);
										        image_width=(500);
										        font_formatting=(no);
										        output_start=();
										        output_row_start=();
										        output_column_start=();
										        output_item=([nested_nested_price]);
										        output_column_end=();
										        output_row_end=();
										        output_end=();
										        output_column_blank=();
									        [/ekm:nested_nested_showdata]
									    </strong>
			                    		<div>
						                    <span class="pence"></span>
						                    <span class="vat">inc vat</span>
						                </div>
						            </span>
						        </div>
			                    <a href="[url]" class="button filled">buy now</a>
					        </div>
			            </div>
			        </div>[editbuttons]';
        output_row_end='';
        output_end='
        		</div>
			</div>';
        output_column_blank='';
    [/ekm:showdata]

    <!-- Sub category panels on electric water page-->
    [ekm:showdata]
        data='categories';
        location='auto';
        includelist='366,369,368';
        excludelist='';
        cols='1';
        rows='100';
        counterreset='1';
        page_navigation='no';
        orderby='auto';
        skiprecords='0';
        editbuttons='YES';
        image_ratio='180';
        image_width='500';
        font_formatting='no';
        output_start='
		    <div class="sub-categories" data-height-determined-by=".image-container,.description h3,.description">';
        output_row_start='';
        output_item='
		       	<div class="category-panel">
		            <div class="image-container">
		                <a href="[url]">
		                    <img src="[image-url]" />
		                </a>
		            </div>
		            <div class="content-container">
		                <div class="description">
		                    <h3>[name-NOEDIT-NOLINK]</h3>
		                    [shortdescription]
		                </div>
		                <div class="price-container">
		                    <div class="price-block">
		                    	<span class="from">kits from just</span>
		                    	<span class="price-value">
		                    		<strong>
		                    			<!-- Output cheapest product price -->
								        [ekm:nested_nested_showdata]
									        data=(product);
									        location=([id]);
									        cols=(1);
									        rows=(1);
									        counterreset=(1);
									        page_navigation=(no);
									        orderby=(price ASC);
									        skiprecords=(0);
									        editbuttons=(NO);
									        image_ratio=(180);
									        image_width=(500);
									        font_formatting=(no);
									        output_start=();
									        output_row_start=();
									        output_column_start=();
									        output_item=([nested_nested_price]);
									        output_column_end=();
									        output_row_end=();
									        output_end=();
									        output_column_blank=();
								        [/ekm:nested_nested_showdata]
								    </strong>
		                    		<div>
					                    <span class="pence"></span>
					                    <span class="vat">inc vat</span>
					                </div>
					            </span>
					        </div>
		                    <a href="[url]" class="button filled">buy now</a>
				        </div>
		            </div>
		        </div>[editbuttons]';
        output_row_end='';
        output_end='
			</div>';
        output_column_blank='';
    [/ekm:showdata]

	<!-- Standard product panels to display sub-categories -->
    [ekm:showdata]
        data='categories';
        location='auto';
        excludelist='360,361,362,366,367,368,369,371,372,373,374,375,386,404,423,434';
        cols='1';
        rows='100';
        counterreset='1';
        page_navigation='no';
        orderby='auto';
        skiprecords='0';
        editbuttons='YES';
        image_ratio='180';
        image_width='500';
        font_formatting='no';
        output_start='
        	<div class="category-panel-container">
				<div data-height-determined-by="h2">';
        output_row_start='';
        output_item='
		        	<div class="product-panel">
			            <a href="[url]">
			                <h2>[name-NOEDIT-NOLINK]</h2>
			                <div class="image-block">
			                    <img src="[image-url]">
			                </div>
			                <div class="button-block">
			                    <div class="price-block">
			                    	<span class="from">from only</span>
			                    	<span class="price-value">
			                    		<strong>
			                    			<!-- Output cheapest product price -->
									        [ekm:nested_nested_showdata]
										        data=(product);
										        location=([id]);
										        cols=(1);
										        rows=(1);
										        counterreset=(1);
										        page_navigation=(no);
										        orderby=(price ASC);
										        skiprecords=(0);
										        editbuttons=(NO);
										        image_ratio=(180);
										        image_width=(500);
										        font_formatting=(no);
										        output_start=();
										        output_row_start=();
										        output_column_start=();
										        output_item=([nested_nested_price]);
										        output_column_end=();
										        output_row_end=();
										        output_end=();
										        output_column_blank=();
									        [/ekm:nested_nested_showdata]
									    </strong>
			                    		<div>
						                    <span class="pence"></span>
						                    <span class="vat">inc vat</span>
						                </div>
						            </span>
						        </div>
			                    <div class="button">SHOP NOW</div>
			                    <img src="/ekmps/shops/[ekm:siteusername][/ekm:siteusername]/resources/design/ufhd.png" class="ufhd-logo">
			                </div>
			            </a>
			        </div>[editbuttons]';
        output_row_end='';
        output_end='
        		</div>
			</div>';
        output_column_blank='';
    [/ekm:showdata]

</div>
<div id="side-bar">

    [ekm:showdata]
        data='categories';
        location='auto';
        orderby='name DESC';
        cols='1';
        rows='100';
        counterreset='1';
        excludelist='429,430,431,432';
        page_navigation='no';
        font_formatting='no';
        output_start='<ul class="side-nav">';
        output_row_start='';
        output_column_start='';
        output_item='
	        <li>[name]
	            [ekm:nested_showdata]
		            data="all";
		            location="[id]";
		            orderby="price ASC, name ASC";
		            cols="1";
		            rows="100";
		            counterreset="1";
		            page_navigation="no";
		            font_formatting="no";
		            output_start="<ul class="sub-category">";
	                output_row_start="";
	                output_column_start="";
	                output_item="<li>[nested_name]</li>";
	                output_column_end="";
	                output_row_end="";
	                output_end="</ul>";
		            output_column_blank="";
	            [/ekm:nested_showdata]
	        </li>';
        output_column_end='';
        output_row_end='';
        output_end='</ul>';
        output_column_blank='';
    [/ekm:showdata]

    [ekm:filter_by][/ekm:filter_by]

    <div class="side-bar-links">
        <a href="confused-224-w.asp" class="link-icon confused">Confused?</a>
        <a href="how-to-measure-up-236-w.asp" class="link-icon measuring">Measuring Up</a>
        <a href="how-to-install-235-w.asp" class="link-icon install">How to install</a>
        <a href="request-a-quote-239-w.asp" class="link-icon quote">Request a quote</a>
    </div>

    <!-- TrustBox widget - Mini -->
    <div class="trustpilot-widget" data-locale="en-GB" data-template-id="53aa8807dec7e10d38f59f32" data-businessunit-id="4eca53ef0000640005119918" data-style-height="150px" data-style-width="100%" data-theme="light">
    	<a href="https://uk.trustpilot.com/review/www.underfloorheating-direct.com" target="_blank">Trustpilot</a>
    </div>
    <!-- End TrustBox widget -->

    <!-- TrustBox widget - Quote -->
	<div class="trustpilot-widget" data-locale="en-GB" data-template-id="54d0e1d8764ea9078c79e6ee" data-businessunit-id="4eca53ef0000640005119918" data-style-height="300px" data-style-width="100%" data-theme="light" data-stars="1,2,3,4,5">
	  <a href="https://uk.trustpilot.com/review/www.underfloorheating-direct.com" target="_blank">Trustpilot</a>
	</div>
	<!-- End TrustBox widget -->

</div>