<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Link_tracker_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function save($post)
    {
        $properties = array(
            'page' => $post['page'],
            'href' => $post['href'],
            'date_time' => date('Y-m-d H:i:s'),
            'link_type' => $post['link_type'],
            'ip' => $this->input->ip_address()
        );
        $this->db->insert('link_tracker', $properties);

        if ($this->db->affected_rows() > 0)
        {
            return true;
        }
        return false;
    }

    public function load($sortKey = FALSE, $sortDirection = 'asc', $searchCriteria = FALSE)
    {
        if (!empty($searchCriteria['to']) && !empty($searchCriteria['from']))
        {
            $this->db->where('unix_timestamp(date_time) <', strtotime($searchCriteria['to']))->where('unix_timestamp(date_time) >', strtotime($searchCriteria['from']));
        }

        if (!empty($searchCriteria['search-key']) && !empty($searchCriteria['search-criteria']))
        {
            $this->db->like($searchCriteria['search-key'], $searchCriteria['search-criteria']);
        }

        if ($sortKey != FALSE)
        {
            $this->db->order_by($sortKey, $sortDirection);
        }

        return $this->db->get('link_tracker')->result();
    }
    
    public function delete ($id)
    {
        $this->db->where('id',$id)->delete('link_tracker');
    }
    
    public function load_info ($page)
    {
        //Find number of times the page has been clicked
        $click_to = $this->db->where('href',$page)->get('link_tracker')->num_rows();
        //Find number of times the page has been clicked from
        $click = $this->db->where('page',$page)->get('link_tracker')->num_rows();
        //Find list of pages clicked from the page with number of times
        $list = $this->db->select('href')->where('page',$page)->get('link_tracker')->result();
        //Add counter
        foreach($list as $item)
        {
            $item->count = $this->db->where('page',$page)->where('href',$item->href)->get('link_tracker')->num_rows();
        }
        //Remove duplicates
        
        //Find list of pages clicked to the page with number of times
        $list_to = $this->db->select('page')->where('href',$page)->get('link_tracker')->result();
        //Return all results as array
        return ["page" => $page,"click" => $click,"click_to" => $click_to,"list" => $list,"list_to" => $list_to];
        
    }
}
