<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Link_tracker extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('link_tracker_model');
    }

    public function track_link()
    {
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        $this->form_validation->set_rules('page', 'Page', 'required|xss_clean|max_length[100]|min_length[1]|matches[link_tracker/]');
        $this->form_validation->set_rules('href', 'HREF', 'required|xss_clean|max_length[255]|min_length[1]|matches[link_tracker/]');
        $this->form_validation->set_rules('link_type', 'Link Type', 'required|xss_clean');

        if ($this->form_validation->run() == FALSE)
        {
            echo validation_errors();
            exit;
        }

        echo $this->link_tracker_model->save($this->input->post()) ? '1' : '0';
    }

    public function view_summary($sortKey = 'id', $sortDirection = FALSE)
    {
        $searchCriteria = FALSE;
        if ($this->input->post())
        {
            $searchCriteria = $this->input->post();
            $this->session->set_userdata('searched', $this->input->post());
        }
        //check for session variable
        else if ($this->session->userdata('searched'))
        {
            //retreive search criteria values from session array
            $searchCriteria = $this->session->userdata('searched');
        }

        //values for view - table headings
        $this->data['sortDir'] = $sortDirection == FALSE ? 'asc' : ($sortDirection == 'asc' ? 'desc' : 'asc');
        $this->data['sortKey'] = $sortKey;
                
        //get record set
        $this->data['results'] = $this->link_tracker_model->load($sortKey, $sortDirection, $searchCriteria);
        //display
        $this->load->view('link_tracker/results', $this->data);
    }
    
    public function view_stats ()
    {
        $this->data['results'] = $this->link_tracker_model->load_info('/');  
        $this->load->view('link_tracker/stats', $this->data);
    }

    public function clear_data()
    {
        $this->session->unset_userdata('searched');
        redirect('/link_tracker/view_summary');
    }
    
    public function delete_row($id)
    {
        $this->link_tracker_model->delete($id);
        redirect('/link_tracker/view_summary');
    }

}
