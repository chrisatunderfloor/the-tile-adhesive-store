<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Page extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Main entry point for dynamically loading the content
     *
     * @param  string $page The filename of the view we want to load
     */
    function index($page = 'home_local')
    {
        if (method_exists($this, $page)) {
            $function_name = $page;
            $this->$function_name();
        } else {
            $this->load($page);
        }
    }

    public function emails($view)
    {
    	$this->load->view('emails/' . $view);
    }

    function search_local() {
    	# Get the page content we are after
        $content = $this->load->view('templates/search_local', $this->data, TRUE);

        # Load the main template
        $output = $this->load->view('templates/main_local', $this->data, TRUE);

        # Replace any EKM specific tags
        $find_what = array('[ekm:content]','[ekm:siteusername][/ekm:siteusername]','[ekm:location][/ekm:location]');
        $replace_with = array($content,'plumbmein','');
        $output = str_replace($find_what, $replace_with, $output);

        $this->output->set_output($output);
    }

    /**
     * Dynamically load the content
     *
     * @param  string 	$page 	The filename of the view we want to load
     */
    public function load($page)
    {
        if($page=='home_local') {
            $page_path = 'templates/';
        } else {
            $type = substr($page, strlen($page)-1);
            switch($type) {
                case 'p':
                    $page_path = 'products/';
                    break;
                case 'c':
                    $page_path = 'categories/';
                    break;
                default:
                    $page_path = 'pages/';
                    break;
            }
        }

        $template_path = 'templates/';

        if (!file_exists(APPPATH . 'views/' . $page_path . $page . '.php')) {
            redirect('/', 'refresh');
        }

        # Get the page content we are after
        $content = $this->load->view($page_path . $page, $this->data, TRUE);

        # Load the main template
        $output = $this->load->view($template_path . 'main_local', $this->data, TRUE);

        # Replace any EKM specific tags
        $find_what = array('[ekm:content]','[ekm:siteusername][/ekm:siteusername]','[ekm:location][/ekm:location]');
        $replace_with = array($content,'plumbmein','');
        $output = str_replace($find_what, $replace_with, $output);

        $this->output->set_output($output);
    }
}